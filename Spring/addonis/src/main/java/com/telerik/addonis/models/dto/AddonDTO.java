package com.telerik.addonis.models.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class AddonDTO {
    @NotBlank(message = "Addon name cannot be empty!")
    @Size(min = 1, max = 64, message = "Name cannot be more than 64 characters long!")
    private String name;

    @Size(max = 255, message = "Description cannot be more than 255 characters long!")
    private String description;

    @Size(max = 255, message = "Origin link cannot be more than 255 characters long!")
    private String origin;

    private String tags;

    public AddonDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
