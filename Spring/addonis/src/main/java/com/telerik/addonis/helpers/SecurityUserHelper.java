package com.telerik.addonis.helpers;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

/**
 * <h1>CurrentUser</h1>
 * CurrentUser is used find the role of the currently logged in User.
 * <p>
 * <b>Note:</b> CurrentUser is a component.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Component
public class SecurityUserHelper {
    UserDetailsManager userDetailsManager;

    public SecurityUserHelper(UserDetailsManager userDetailsManager) {
        this.userDetailsManager = userDetailsManager;
    }

    /**
     * This method determines the currently logged in User through Spring Security
     *
     * @return A String of the username of the currently logged in User.
     */
    public String getCurrentUserName() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        } else {
            return "";
        }
    }

    /**
     * This method determines whether the given user is of ROLE_ADMIN authority, using Spring Security
     *
     * @param username This is a String corresponding to the username of the User in the database.
     * @return true or false if the user is or is not of the ROLE_ADMIN authority
     */
    public boolean isUserAdmin(String username) {
        if (userDetailsManager.userExists(username)) {
            Object[] roles = userDetailsManager.loadUserByUsername(username).getAuthorities().toArray();
            for (Object role : roles) {
                if (role.toString().equalsIgnoreCase("ROLE_ADMIN")) {
                    return true;
                }
            }
        }
        return false;
    }
}
