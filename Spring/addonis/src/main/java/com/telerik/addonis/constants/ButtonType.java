package com.telerik.addonis.constants;

/**
 * <h1>ButtonType</h1>
 * ButtonType is used to provide string constants denoting button types provided to the controllers
 * by the html templates.
 *
 * @author Ivaylo Staykov, Alexander Stoychev
 * @version 1.0
 * @since 2020-04-15
 */
public interface ButtonType {
    String UPDATE_USER = "Update User";
    String UPDATE = "Update";
    String CREATE_ADDON = "Create Add-on";
    String DISABLE_USER = "Disable User";
    String ENABLE_USER = "Enable User";
    String DELETE = "Delete";
    String VIEW_USERS = "View All Users";
    String INDEX = "Index";
    String HOME = "Home";
    String PERSONAL_PANEL = "Personal Panel";
    String CREATE_TAG = "Create Tag";
    String UPDATE_TAG = "Update Tag";
    String DELETE_TAG = "Delete Tag";
    String ALL_TAGS = "View All Tags";
}
