package com.telerik.addonis.controllers.rest;

import com.telerik.addonis.models.Content;
import com.telerik.addonis.services.contracts.AddonService;
import com.telerik.addonis.services.contracts.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FileRestController {
    private final ContentService contentService;
    private final AddonService addonService;

    @Autowired
    public FileRestController(ContentService contentService, AddonService addonService) {
        this.contentService = contentService;
        this.addonService = addonService;
    }

    @GetMapping("/addon/download/{addonName}")
    public ResponseEntity<byte[]> download(@PathVariable("addonName") String addonName) {
        Content content = contentService.getByName(addonName);
        addonService.updateNumberOfDownloads(addonName);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + content.getName() +
                content.getType() + "\"")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(content.getFile());
    }
}
