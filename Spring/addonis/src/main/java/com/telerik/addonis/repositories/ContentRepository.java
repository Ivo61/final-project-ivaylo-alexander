package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Content;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <h1>ContentRepository</h1>
 * ContentRepository is used to access our database.
 * <p>
 * <b>Note:</b> ContentRepository extends the JpaRepository interface.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Repository
public interface ContentRepository extends JpaRepository<Content, Integer> {
    /**
     * This method is used to retrieve a Content object from the repository
     * by the given name field of the object.
     *
     * @param name This is the name of the Content object
     * @return This method returns the Content object.
     */
    Content findByName(String name);

    /**
     * This method is used to check if a specific Content exists.
     *
     * @param contentName This is the name of the Content object
     * @return This boolean variable tells us if the Content exists.
     */
    boolean existsByName(String contentName);
}
