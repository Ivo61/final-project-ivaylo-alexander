package com.telerik.addonis.models.vo;

import com.telerik.addonis.models.Addon;

/**
 * <h1>AddonWithRating</h1>
 * AddonWithRating class represents an addon object with a calculated average rating.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */

public class AddonWithRatingVO {
    /**
     * This is an Addon field, representing the Addon being rated
     */
    private Addon addon;

    /**
     * This is an Double field, representing average rating of the Addon object.
     */
    private Double averageRating;

    /**
     * This is a default constructor for AddonWithRating objects.
     */
    public AddonWithRatingVO() {
    }

    /**
     * This is a constructor for AddonWithRating objects that recieves values for it's fields.
     *
     * @param addon         is the Addon object being rated.
     * @param averageRating is the value of the all ratings for this Addon, averaged.
     */
    public AddonWithRatingVO(Addon addon, Double averageRating) {
        this.addon = addon;
        this.averageRating = averageRating;
    }

    /**
     * This is the getter for the addon field.
     *
     * @return This returns the Addon of the AddonWithRating object.
     */
    public Addon getAddon() {
        return addon;
    }

    /**
     * This is the setter for the addon field.
     *
     * @param addon This is an Addon, which is going to be the addon of the AddonWithRating object.
     */
    public void setAddon(Addon addon) {
        this.addon = addon;
    }

    /**
     * This is the getter for the averageRating field.
     *
     * @return This returns the averageRating field of the AddonWithRating object.
     */
    public Double getAverageRating() {
        return averageRating;
    }

    /**
     * This is the setter for the averageRating field.
     *
     * @param averageRating This is a Double, which is going to be the averageRating of the AddonWithRating object.
     */
    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }
}
