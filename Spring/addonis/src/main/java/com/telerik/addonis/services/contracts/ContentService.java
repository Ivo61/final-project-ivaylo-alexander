package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.Content;

/**
 * <h1>ContentService</h1>
 * ContentService is used to give the logic of our application regarding Content objects.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
public interface ContentService {
    /**
     * This method is used to retrieve a specific Content object by its name.
     *
     * @param name This is the name of the Content object
     * @return This returns a Content object corresponding to the name parameter.
     */
    Content getByName(String name);

    /**
     * This method is used to create a Content object.
     *
     * @param content This is a Content object
     * @return This returns the created Content object.
     */
    Content create(Content content);
}
