package com.telerik.addonis.controllers.rest;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.helpers.AddonMapper;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.dto.AddonDTO;
import com.telerik.addonis.repositories.GitHubApiRepositoryImpl;
import com.telerik.addonis.services.contracts.AddonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

/**
 * <h1>AddonRestController</h1>
 * AddonRestController is a REST controller that provides CRUD operations for Addon objects
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@RestController
@RequestMapping("/api/addons")
public class AddonRestController {
    /**
     * The service that has access to the database and to the information gathered from gitHub.
     */
    private final AddonService addonService;
    private final GitHubApiRepositoryImpl gitHubApiRepositoryImpl;
    private final AddonMapper mapper;

    /**
     * This is the constructor for TagRestController objects.
     *
     * @param addonService            This is the service containing the the logic behind the CRUD operations.
     * @param gitHubApiRepositoryImpl This is the service gathering information from gitHub.
     */
    @Autowired
    public AddonRestController(AddonService addonService, GitHubApiRepositoryImpl gitHubApiRepositoryImpl, AddonMapper mapper) {
        this.addonService = addonService;
        this.gitHubApiRepositoryImpl = gitHubApiRepositoryImpl;
        this.mapper = mapper;
    }

    /**
     * This method is used to get all Addon objects in our database as a List
     *
     * @return List of the Addon objects
     */
    @GetMapping
    public List<Addon> getAll() {
        return addonService.getAll();
    }

    /**
     * This method is used to create an object of type Addon.
     *
     * @param newAddon This is an object that has: int id, string name, string description, Set<Content> content
     *                 String origin, int numberOfDownloads, Date uploadDate, String creator, Set<Status> status,
     *                 Set<Tag> tags0.
     * @param file     This is a MultipartFile containing the Addon's downloadable file
     * @return This returns the Tag object that was created.
     */
    @PostMapping
    public Addon create(@RequestBody AddonDTO newAddon, @RequestParam(value = "tags", required = false) String tags, MultipartFile file) throws IOException {
        try {
            return addonService.create(mapper.DTOToAddon(newAddon, file));
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    /**
     * This method is used to update an existing Addon.
     *
     * @param editedAddon This is the object we want to change.
     * @param file        This is the MultipartFile for the updated object.
     * @return This returns the Tag object that was edited.
     */
    @PutMapping("/{id}")
    public Addon update(@PathVariable(name = "id") int id, @RequestBody AddonDTO editedAddon, MultipartFile file) {
        try {
            return addonService.update(id, mapper.updateAddon(addonService.getById(id), editedAddon, file));
        } catch (EntityNotFoundException | IOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * This method is used to delete an existing Tag.
     *
     * @param addonToDelete This is a string that corresponds to an Addon with the same name in our database.
     * @return This returns the Tag object that was deleted.
     */
    @DeleteMapping("/{name}")
    public Addon delete(@PathVariable("name") String addonToDelete) {
        try {
            return addonService.delete(addonToDelete);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * This method is used to retrieve the number of issues the product has on gitHub.
     *
     * @param url This is a string that corresponds to the address of the product on gitHub.
     * @return This returns an int with the number of issues.
     * @throws IOException when a connection to could not be made using the url.
     */
    @GetMapping("/issues")
    public int getTotalIssueCount(@RequestBody String url) throws IOException {
        return gitHubApiRepositoryImpl.getOpenIssueCount(url);
    }

    /**
     * This method is used to retrieve the number of pulls the product has on gitHub.
     *
     * @param url This is a string that corresponds to the address of the product on gitHub.
     * @return This returns an int with the number of pulls.
     * @throws IOException when a connection could not be made using the url.
     */
    @GetMapping("/pulls")
    public int getPullRequestCount(@RequestBody String url) throws IOException {
        return gitHubApiRepositoryImpl.getPullRequestsCount(url);
    }

    /**
     * This method is used to retrieve the last date the product on gitHub has had a commit.
     *
     * @param url This is a string that corresponds to the address of the product on gitHub.
     * @return This returns a Date object with the date of the last commit.
     * @throws IOException when a connection could not be made using the url.
     */
    @GetMapping("/lastcommitdate")
    public String getDate(@RequestBody String url) throws IOException {
        return gitHubApiRepositoryImpl.getLastCommitDate(url);
    }

    /**
     * This method is used to retrieve the headline of the last commit the product has had on gitHub.
     *
     * @param url This is a string that corresponds to the address of the product on gitHub.
     * @return This returns a String with the headline of the last commit.
     * @throws IOException when a connection could not be made using the url
     */
    @GetMapping("/lastcommittitle")
    public String getTitle(@RequestBody String url) throws IOException {
        return gitHubApiRepositoryImpl.getLastCommitTitle(url);
    }
}
