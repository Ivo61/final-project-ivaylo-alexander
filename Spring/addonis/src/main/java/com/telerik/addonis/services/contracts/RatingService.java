package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.Rating;

import java.util.List;

/**
 * <h1>RatingService</h1>
 * RatingService is used to give the logic of our application regarding Rating objects.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
public interface RatingService {
    /**
     * This method is used associate a Rating object to an Addon in our database.
     *
     * @param addonName this is the name of the Addon object that is to be rated.
     * @param score     this is the score being associated with the Addon object.
     * @return This returns the Rating object that has been added.
     */
    Rating rate(String addonName, String score);

    /**
     * This method is used to retrieve all Rating objects in our database.
     *
     * @return This returns all of the Rating objects in our database as a list.
     */
    List<Rating> getAll();

    /**
     * This method is used retrieve the average of all the ratings associated with a certain Addon object.
     *
     * @param addonName this is the name field of the Addon whose average rating is being retrieved.
     * @return This returns a double value of the average rating.
     */
    double getAverageRatingForAddon(String addonName);

}
