package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.Rating;
import com.telerik.addonis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <h1>RatingRepository</h1>
 * RatingRepository is used to access our database.
 * <p>
 * <b>Note:</b> RatingRepository extends the JpaRepository interface.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Repository
public interface RatingRepository extends JpaRepository<Rating, Integer> {
    /**
     * This method is used to check if a specific rating for a specific addon and user exists.
     *
     * @param addon This is an Addon object
     * @param user  This is an User object
     * @return This tells us if the Rating exists.
     */
    boolean existsByAddonAndUser(Addon addon, User user);

    /**
     * This method is used to give us a list of ratings for an addon.
     *
     * @param name This is an Addon name field.
     * @return This is a list of all the ratings for an addon.
     */
    List<Rating> getByAddonName(String name);

    /**
     * This method is used to give us a specific Rating for an addon rated by a specific user.
     *
     * @param addon This is an Addon object
     * @param user  This is an User object
     * @return This returns a Rating object.
     */
    Rating getByAddonAndUser(Addon addon, User user);
}
