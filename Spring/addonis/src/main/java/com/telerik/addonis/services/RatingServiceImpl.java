package com.telerik.addonis.services;

import com.telerik.addonis.helpers.SecurityUserHelper;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.Rating;
import com.telerik.addonis.models.User;
import com.telerik.addonis.repositories.RatingRepository;
import com.telerik.addonis.services.contracts.AddonService;
import com.telerik.addonis.services.contracts.RatingService;
import com.telerik.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <h1>RatingServiceImpl</h1>
 * RatingServiceImpl iis used to give access to the database when dealing with objects of the Rating class,
 * it also provides the logic.
 * <p>
 * <b>Note:</b> RatingServiceImpl implements the RatingService interface.
 * <b>Note:</b> This is a @Service.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Service
public class RatingServiceImpl implements RatingService {
    /**
     * These are all the @Components and @Services we need in order to implement our logic.
     */
    private final RatingRepository repository;
    private final AddonService addonService;
    private final UserService userService;
    private final SecurityUserHelper currentUser;

    /**
     * This is the constructor for the RatingServiceImpl objects, generating all the @Components and @Services needed.
     */
    @Autowired
    public RatingServiceImpl(RatingRepository repository, AddonService addonService, UserService userService, SecurityUserHelper currentUser) {
        this.repository = repository;
        this.addonService = addonService;
        this.userService = userService;
        this.currentUser = currentUser;
    }

    /**
     * This method is used associate a Rating object to an Addon object in our database.
     *
     * @param addonName This is the name of the Addon object that is to be rated.
     * @param score     This is the score being associated with the Addon object.
     * @return This returns the Rating object that has been added.
     */
    @Override
    public Rating rate(String addonName, String score) {
        User user = userService.getByName(currentUser.getCurrentUserName());
        Addon addon = addonService.getByName(addonName);
        Rating rating = new Rating();
        rating.setUser(user);
        rating.setAddon(addon);
        rating.setRating(Integer.parseInt(score));
        if (repository.existsByAddonAndUser(addon, user)) {
            rating.setId(repository.getByAddonAndUser(addon, user).getId());
        }
        return repository.save(rating);
    }

    /**
     * This method is used to retrieve all Rating objects in our database.
     *
     * @return This returns all of the Rating objects in our database as a list.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Rating> getAll() {
        return repository.findAll();
    }

    /**
     * This method is used retrieve the average of all the ratings associated with a certain Addon object.
     *
     * @param addonName this is the name field of the Addon whose average rating is being retrieved.
     * @return This returns a double value of the average rating.
     */
    @Override
    @Transactional(readOnly = true)
    public double getAverageRatingForAddon(String addonName) {
        List<Integer> average = repository.getByAddonName(addonName)
                .stream()
                .map(Rating::getRating)
                .collect(Collectors.toList());
        return average
                .stream()
                .mapToDouble((x) -> x)
                .average().orElse(0);
    }

}
