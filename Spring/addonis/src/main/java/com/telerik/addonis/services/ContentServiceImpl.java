package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Content;
import com.telerik.addonis.repositories.ContentRepository;
import com.telerik.addonis.services.contracts.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <h1>ContentServiceImpl</h1>
 * ContentServiceImpl is used to give access to the database when dealing with
 * objects of the Content Class, it also provides the logic.
 * <p>
 * <b>Note:</b> ContentServiceImpl implements the ContentService interface.
 * <b>Note:</b> This is a @Service.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Service
public class ContentServiceImpl implements ContentService {
    /**
     * The repository that stores the Content objects.
     */
    ContentRepository repository;

    @Autowired
    public ContentServiceImpl(ContentRepository repository) {
        this.repository = repository;
    }

    /**
     * This method is used to retrieve a Content object from the repository
     * by the given name field of the object.
     *
     * @param name This is the name of the Content object
     * @return This returns a Content object.
     */
    @Override
    @Transactional(readOnly = true)
    public Content getByName(String name) {
        if (!repository.existsByName(name)) {
            throw new EntityNotFoundException("Status", name);
        }
        return repository.findByName(name);
    }

    /**
     * This method is used to create a Content object and save it into the database.
     *
     * @param content This is a Content object
     * @return This returns a Content object.
     */
    @Override
    public Content create(Content content) {
        return repository.save(content);
    }
}
