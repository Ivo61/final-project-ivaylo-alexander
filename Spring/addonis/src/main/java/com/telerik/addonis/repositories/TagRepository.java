package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

/**
 * <h1>TagRepository</h1>
 * TagRepository is used to access our database.
 * <p>
 * <b>Note:</b> TagRepository extends the JpaRepository interface.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
public interface TagRepository extends JpaRepository<Tag, Integer> {
    /**
     * This method is used to retrieve a Tag object from the repository
     * by the given name field of the object.
     *
     * @param tagName This is the name of the Tag object
     * @return This returns the Tag object.
     */
    Tag findByName(String tagName);

    /**
     * This method is used to check if a specific Tag exists.
     *
     * @param tagName This is the name of the Tag object
     * @return This tells us if the Tag exists.
     */
    boolean existsByName(String tagName);

    /**
     * This method is used to delete a specific tag.
     *
     * @param name This is the name of the Tag object
     */
    @Modifying
    void deleteByName(String name);
}
