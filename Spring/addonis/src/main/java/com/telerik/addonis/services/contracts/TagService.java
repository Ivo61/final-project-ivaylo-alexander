package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.Tag;

import java.util.List;

/**
 * <h1>TagService</h1>
 * TagService is used to give the logic of our application regarding Tag objects.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
public interface TagService {
    /**
     * This method is used to retrieve all Tags in our database.
     *
     * @return This returns all of the Tag objects in our database as a list.
     */
    List<Tag> getAll();

    /**
     * This method is used to retrieve a specific Tag by its name.
     *
     * @param tagName This is the name of the Tag object
     * @return This returns the Tag object corresponding to the name given.
     */
    Tag getByName(String tagName);

    /**
     * This method is used to create a Tag.
     *
     * @param tag This is a Tag object containing the data for the fields of the Tag being created.
     * @return This returns the created Tag object.
     */
    Tag create(Tag tag);

    /**
     * This method is used to update an existing Tag.
     *
     * @param tag This is the Tag object that is to be updated.
     * @return This returns the edited Tag object.
     */
    Tag update(Tag tag);

    /**
     * This method is used to delete an existing Tag by its name.
     *
     * @param tagName This is the name of the Tag object.
     * @return This returns the deleted Tag object.
     */
    Tag delete(String tagName);
}
