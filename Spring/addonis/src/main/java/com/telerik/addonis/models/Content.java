package com.telerik.addonis.models;

import javax.persistence.*;

/**
 * <h1>Content</h1>
 * Content class represents content in the database.
 * <p>
 * <b>Note:</b> Content is an Entity.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Entity
@Table(name = "content")
public class Content {

    /**
     * This is the id of the entity class, which is an auto generated value.
     * The column which corresponds to this field is "id" in the "content" table.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * This is a String field, representing the name of the Content.
     * The column which corresponds to this field is "name" in the "content" table.
     */
    @Column(name = "name")
    private String name;

    /**
     * This is an array of bytes field, representing the file being stored in the Content object.
     * The column which corresponds to this field is "file" in the "content" table.
     */
    @Lob
    @Column(name = "file")
    private byte[] file;

    /**
     * This is a String field, representing the extension of the file being stored in the Content object.
     * The column which corresponds to this field is "type" in the "content" table.
     */
    @Column(name = "type")
    private String type;

    /**
     * This is a default constructor for Content objects.
     */
    public Content() {
    }

    /**
     * This is the getter for the id field.
     *
     * @return This returns the id of the Content object.
     */
    public int getId() {
        return id;
    }

    /**
     * This is the setter for the id field.
     *
     * @param id This is an int, which is going to be the id of the object Content.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * This is the getter for the name field.
     *
     * @return This returns the name of the Content object.
     */
    public String getName() {
        return name;
    }

    /**
     * This is the setter for the name field.
     *
     * @param name This is a String, which is going to be the name of the object Content.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This is the getter for the file field.
     *
     * @return This returns the file of the Content object.
     */
    public byte[] getFile() {
        return file;
    }

    /**
     * This is the setter for the file field.
     *
     * @param content This is an array of bytes, which is going to be the file data of the Content object.
     */
    public void setFile(byte[] content) {
        this.file = content;
    }

    /**
     * This is the getter for the type field.
     *
     * @return This returns the type of the Content object.
     */
    public String getType() {
        return type;
    }

    /**
     * This is the setter for the type field.
     *
     * @param type This is a String, which is going to be the file extension of the file being stored
     *             in the Content object.
     */
    public void setType(String type) {
        this.type = type;
    }
}
