package com.telerik.addonis.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

/**
 * <h1>User</h1>
 * User class represents Users in the database.
 * <p>
 * <b>Note:</b> User is an Entity.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Entity
@Table(name = "users")
public class User {
    /**
     * This is the primary key of the entity class, which is a String and the name of the user.
     * The column which corresponds to this field is "username" in the "users" table.
     */
    @Id
    @Column(name = "username")
    @Size(min = 3, max = 50, message = "Username must be between 3 and 50 characters.")
    private String username;

    /**
     * This is a String field, representing the password of the user, represented by the User object.
     * The column which corresponds to this field is "password" in the "users" table.
     */
    @Column(name = "password")
    @Size(min = 4, max = 68, message = "Password must be between 4 and 68 characters.")
    private String password;

    /**
     * This is a String field, representing the e-mail of the user, represented by the User object.
     * The column which corresponds to this field is "email" in the "users" table.
     */
    @Column(name = "email")
    @Email
    private String email;

    /**
     * This is a byte field, representing whether the User object is enabled or disabled to log into the
     * authentication system.
     * The column which corresponds to this field is "enabled" in the "users" table.
     */
    @Column(name = "enabled")
    private byte enabled;

    /**
     * This is a String field, representing the confirmation of the password, necessary when logging in
     * as a user with the User object.
     * There is no column in the "users" table corresponding to this field.
     */
    @Transient
    private String passwordConfirmation;

    /**
     * This is a default constructor for Content objects.
     */
    public User() {
    }

    /**
     * This is the getter for the username field.
     *
     * @return This returns the username of the User object.
     */
    public String getUsername() {
        return username;
    }

    /**
     * This is the setter for the username field.
     *
     * @param username This is a String, which is going to be the username of the User object.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * This is the getter for the password field.
     *
     * @return This returns the password of the User object.
     */
    public String getPassword() {
        return password;
    }

    /**
     * This is the setter for the password field.
     *
     * @param password This is a String, which is going to be the password of the User object.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * This is the getter for the email field.
     *
     * @return This returns the email of the User object.
     */
    public String getEmail() {
        return email;
    }

    /**
     * This is the setter for the email field.
     *
     * @param email This is a String, which is going to be the e-mail of the User object.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * This is the getter for the username field.
     *
     * @return This returns the username of the User object.
     */
    public byte getEnabled() {
        return enabled;
    }

    /**
     * This is the setter for the enabled field.
     *
     * @param enabled This is a byte, which is going to be the enabled flag of the User object.
     */
    public void setEnabled(byte enabled) {
        this.enabled = enabled;
    }

    /**
     * This is the getter for the passwordConfirmation field.
     *
     * @return This returns the passwordConfirmation of the User object.
     */
    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    /**
     * This is the setter for the passwordConfirmation field.
     *
     * @param passwordConfirmation This is a String, which is going to be the confirmed password of
     *                             the User object.
     */
    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
