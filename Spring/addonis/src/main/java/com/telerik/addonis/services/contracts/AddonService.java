package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;

/**
 * <h1>AddonService</h1>
 * AddonService is used to give the logic of our application regarding addons.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
public interface AddonService {
    /**
     * This method is used to retrieve all Addons in our database.
     *
     * @return This returns all of the addons in our database as a list.
     */
    List<Addon> getAll();

    /**
     * This method is used to retrieve a specific Addon by its name.
     *
     * @param addonName This is the name of the Addon object
     * @return This returns a Addon object.
     */
    Addon getByName(String addonName);

    Addon getById(Integer id);

    /**
     * This method is used to create an Addon.
     *
     * @param addon This is an Addon object
     * @return This returns a Addon object.
     */
    Addon create(Addon addon) throws IOException;

    /**
     * This method is used to update an existing Addon.
     *
     * @param addon This is an Addon object
     * @return This returns a Addon object.
     */
    Addon update(int id, Addon addon);

    /**
     * This method is used to update an existing Addon in our database.
     *
     * @param addonToBeUpdated This is an Addon object, that is to be edited.
     * @return This returns the edited Addon object.
     */
    Addon update(Addon addonToBeUpdated);

    /**
     * This method is used to delete an existing Addon by its name.
     *
     * @param addonName This is the name of the Addon object.
     * @return This returns a Addon object.
     */
    Addon delete(String addonName);

    /**
     * This method is used to retrieve a list of all addons with a specific User and Status.
     *
     * @param status   This is the status of the Addon object.
     * @param username This is the creator of the Addon object.
     * @return This returns a list of Addon objects.
     */
    List<Addon> getByUserAndStatus(String status, String username);

    /**
     * This method is used to retrieve a list of addons with a specific User.
     *
     * @param addons   This is a list of addons from which to check for.
     * @param username This is the creator of the Addon object.
     * @return This returns a list of Addon objects.
     */
    List<Addon> filterByUser(List<Addon> addons, String username);

    /**
     * This method is used to retrieve a list of all addons with a specific Status.
     *
     * @param addons      This is a list of addons from which to check for.
     * @param addonStatus This is the status of the Addon object.
     * @return This returns a list of Addon objects.
     */
    List<Addon> filterByStatus(List<Addon> addons, String addonStatus);

    /**
     * This method is used to retrieve a list of all addons with an upload date that is between the current moment
     * and a given point back in time.
     *
     * @param addons       This is a list of addons from which to check for.
     * @param timeInterval This is the Long in milliseconds defining how far back in time to check for.
     * @return This returns a list of Addon objects rated above the given value.
     */
    List<Addon> filterByCreationDate(List<Addon> addons, Long timeInterval);

    /**
     * This method is used to retrieve a list of all addons, sorted by their number of downloads
     *
     * @param addons This is a list of addons from which to check for.
     * @return This returns a list of Addon objects, sorted in descending order of their numberOfDownloads field.
     */
    List<Addon> sortByDownloadCount(List<Addon> addons);

    /**
     * This method is used to change the Set of Status objects of an Addon so that it contains the Featured Status if
     * it does not already, or to remove it if it already contains it.
     *
     * @param addonName This is the name of the Addon being edited.
     */
    Addon toggleFeatured(String addonName);

    /**
     * This method is used to change the Set of Status objects of an Addon so that it no longer contains a Pending status
     * and contains an Approved Status.
     *
     * @param addonName This is the name of the Addon being edited.
     */
    Addon approve(String addonName);

    /**
     * This method is used to change the Set of Status objects of an Addon so that it no longer contains a Pending status
     * and contains an Approved Status.
     *
     * @param addonName This is the name of the Addon being edited.
     */
    Addon reject(String addonName);

    /**
     * This method gives the names of all the statuses that an Addon object has.
     *
     * @param addonName This is the name of the Addon whose Status names are being retrieved.
     * @return This returns a String containing the name fields of the Status objects associated with
     * the given Addon.
     */
    String getStatusNames(String addonName);

    /**
     * This method gives the names of all the tags that an Addon object has.
     *
     * @param addonName This is the name of the Addon whose Tag names are being retrieved.
     * @return This returns a String containing the name fields of the Tag objects associated with
     * the given Addon.
     */
    String getTagNames(String addonName);

    Page<Addon> paginationWithSortByStatus(String sort, int order, Pageable pageable);

    Addon updateNumberOfDownloads(String addonName);

    List<Addon> filter(String keyword, String whereToLook);

    void removeTagFromAddons(Tag tag);

}
