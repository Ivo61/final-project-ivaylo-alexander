package com.telerik.addonis.models;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * <h1>Tag</h1>
 * Tag this class represents tags in the database.
 * <p>
 * <b>Note:</b> Rating is an Entity.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Entity
@Table(name = "tags")
public class Tag {
    /**
     * This is the id of the entity class, which is an auto generated value.
     * The column which corresponds to this field is "id".
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * This is an string field, which is the name of the Tag.
     * The column which corresponds to this field is "name".
     */
    @Column(name = "name")
    @NotBlank(message = "Tag name is required.")
    @Size(max = 50, message = "Name of a tag cannot be more than 50 characters.")
    private String name;

    public Tag() {
    }

    /**
     * This is the getter for id.
     *
     * @return This returns the id of the object Tag.
     */
    public int getId() {
        return id;
    }

    /**
     * This is the setter for id.
     *
     * @param id This is a int, which is going to be the id of the object Tag.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * This is the getter for name.
     *
     * @return This returns the name of the object Rating.
     */
    public String getName() {
        return name;
    }

    /**
     * This is the setter for name.
     *
     * @param name This is a string that is going to be the name of the object Tag.
     */
    public void setName(String name) {
        this.name = name;
    }
}
