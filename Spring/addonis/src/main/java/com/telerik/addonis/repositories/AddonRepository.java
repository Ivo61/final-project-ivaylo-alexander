package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Addon;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * <h1>AddonRepository</h1>
 * AddonRepository is used to access our database.
 * <p>
 * <b>Note:</b> AddonRepository extends the JpaRepository interface.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Repository
public interface AddonRepository extends JpaRepository<Addon, Integer> {
    /**
     * This method is used to retrieve a Addon object from the repository
     * by the given name field of the object.
     *
     * @param AddonName This is the name of the Addon object
     * @return This method returns the Addon object.
     */
    Addon findByName(String AddonName);

    /**
     * This method is used to retrieve a Addon object from the repository
     * by the given id field of the object.
     *
     * @param id This is the id of the Addon object
     * @return This method returns an Optional of Addon object.
     */
    Optional<Addon> findById(Integer id);

    /**
     * This method is used to check if a specific addon exists.
     *
     * @param AddonName This is the name of the Addon object
     * @return This boolean variable tells us if the Addon exists.
     */
    boolean existsByName(String AddonName);

    /**
     * This method is used to check if a specific addon exists.
     *
     * @param id This is the id of the Addon object
     * @return This boolean variable tells us if the Addon exists.
     */
    boolean existsById(Integer id);

    List<Addon> findByNameContainingIgnoreCase(String name);

    Page<Addon> findAllByStatuses_NameOrderByNameAsc(String statuses_name, Pageable pageable);

    Page<Addon> findAllByStatuses_NameOrderByNameDesc(String statuses_name, Pageable pageable);

    Page<Addon> findAllByStatuses_NameOrderByNumberOfDownloadsAsc(String statuses_name, Pageable pageable);

    Page<Addon> findAllByStatuses_NameOrderByNumberOfDownloadsDesc(String statuses_name, Pageable pageable);

    Page<Addon> findAllByStatuses_NameOrderByUploadDateAsc(String statuses_name, Pageable pageable);

    Page<Addon> findAllByStatuses_NameOrderByUploadDateDesc(String statuses_name, Pageable pageable);


}
