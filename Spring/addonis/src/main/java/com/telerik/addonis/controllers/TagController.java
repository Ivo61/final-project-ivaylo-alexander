package com.telerik.addonis.controllers;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.helpers.PaginationHelper;
import com.telerik.addonis.helpers.SecurityUserHelper;
import com.telerik.addonis.models.Tag;
import com.telerik.addonis.services.contracts.AddonService;
import com.telerik.addonis.services.contracts.TagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.telerik.addonis.constants.ButtonType.*;
import static com.telerik.addonis.constants.LoggerMessages.*;

/**
 * <h1>TagController</h1>
 * TagController is an MVC Controller that provides us with our front-end logic and templates.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Controller
public class TagController {
    /**
     * These are all the @Components and @Services we need in order to implement our logic.
     */
    private final Logger logger = LoggerFactory.getLogger(TagController.class);
    private final TagService tagService;
    private final AddonService addonService;
    private final SecurityUserHelper securityUserHelper;

    /**
     * This is the constructor for TagController objects.
     *
     * @param tagService This is the service containing the the logic for manipulating Tags.
     */
    @Autowired
    public TagController(TagService tagService, SecurityUserHelper securityUserHelper, AddonService addonService) {
        this.tagService = tagService;
        this.securityUserHelper = securityUserHelper;
        this.addonService = addonService;
    }

    @GetMapping("/tag/create")
    public String showCreateFrom(Model model) {
        logger.info(String.format(securityUserHelper.getCurrentUserName()+USER_ENTERED, "create tag", "/tag/create"));
        PaginationHelper.resetSort();
        model.addAttribute("tag", new Tag());
        model.addAttribute("username", securityUserHelper.getCurrentUserName());
        return "tag-create";
    }

    @PostMapping("tag/create")
    public String createTag(@Valid @ModelAttribute("tag") Tag tag, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            return "tag-create";
        }
        try {
            tagService.create(tag);
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            logger.error(String.format(securityUserHelper.getCurrentUserName()+DUPLICATE_ENTITY, "create", "tag", tag.getName()));
            return "tag-create";
        }
        return "redirect:/tags";
    }

    @GetMapping("/tag/edit/{name}")
    public String showEditForm(@PathVariable String name, Model model) {
        logger.info(String.format(securityUserHelper.getCurrentUserName()+USER_ENTERED, "edit tag", "/tag/edit" + name));
        PaginationHelper.resetSort();
        model.addAttribute("tag", tagService.getByName(name));
        model.addAttribute("oldname", name);
        model.addAttribute("username", securityUserHelper.getCurrentUserName());
        return "tag-edit";
    }

    @RequestMapping(value = "/tag/edit/{name}")
    public String edit(@Valid @ModelAttribute("tag") Tag tag,
                       @RequestParam("action") String buttonType,
                       @RequestParam("oldname") String oldName,
                       BindingResult errors,
                       Model model) {
        if (errors.hasErrors() && !buttonType.equals("Index")) {
            return "tag-edit";
        }
        switch (buttonType) {
            case UPDATE:
                try {
                    Tag updatingTag = tagService.getByName(oldName);
                    updatingTag.setName(tag.getName());
                    tagService.update(updatingTag);
                    return "redirect:/tags";
                } catch (EntityNotFoundException e) {
                    model.addAttribute("error", e.getMessage());
                    logger.error(String.format(securityUserHelper.getCurrentUserName()+ENTITY_NOT_FOUND, "update", "tag", tag.getName()));
                    return "redirect:/tag/edit/" + tag.getName();
                }
            case DELETE:
                try {
                    addonService.removeTagFromAddons(tagService.getByName(oldName));
                    tagService.delete(oldName);
                    return "redirect:/tags";
                } catch (EntityNotFoundException e) {
                    model.addAttribute("error", e.getMessage());
                    logger.error(String.format(securityUserHelper.getCurrentUserName()+ENTITY_NOT_FOUND, "delete", "tag", tag.getName()));
                    return "redirect:/tag/edit/" + tag.getName();
                }
            case HOME:
                return "redirect:/";
            default:
                return "redirect:/tags";
        }
    }

    /**
     * This method provides a table with all tags in our application
     * <p>
     * <b>Note:</b> The url for this is: /tags
     * <p>
     *
     * @param model This is used to add attributes to the template.
     * @return addons - This is the template to which the attributes must go.
     */
    @GetMapping("/tags")
    public String showAllTags(Model model) {
        logger.info(String.format(securityUserHelper.getCurrentUserName()+USER_ENTERED, "all tags", "/tags"));
        PaginationHelper.resetSort();
        List<Tag> allTags = tagService.getAll();
        model.addAttribute("tags", allTags);
        model.addAttribute("username", securityUserHelper.getCurrentUserName());
        return "tags";
    }

    @RequestMapping("/tags")
    public String redirect(@RequestParam("action") String buttonType, Model model) {
        switch (buttonType) {
            case HOME:
                return "index";
            case CREATE_TAG:
                model.addAttribute("tag", new Tag());
                return "tag-create";
            case PERSONAL_PANEL:
                return "redirect:user";
            default:
                return "index";
        }
    }
}
