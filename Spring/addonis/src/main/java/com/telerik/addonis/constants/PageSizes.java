package com.telerik.addonis.constants;

/**
 * <h1>PageSizes</h1>
 * PageSizes is used to provide Integer constants denoting number of elements shown in tables, card holders.
 *
 * @author Ivaylo Staykov, Alexander Stoychev
 * @version 1.0
 * @since 2020-04-15
 */
public interface PageSizes {
    Integer DEFAULT_PAGE_LIMIT = 5;
    Integer DEFAULT_CARD_LIMIT = 10;
}
