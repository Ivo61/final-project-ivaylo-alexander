package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Status;
import com.telerik.addonis.repositories.StatusRepository;
import com.telerik.addonis.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <h1>StatusServiceImpl</h1>
 * StatusServiceImpl is used to give access to the database when dealing with
 * objects of the Status Class.
 * <p>
 * <b>Note:</b> StatusServiceImpl implements the StatusService interface.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Service
public class StatusServiceImpl implements StatusService {
    /**
     * The repository that stores the Status objects.
     */
    StatusRepository repository;

    /**
     * This is the constructor for StatusServiceImpl objects.
     *
     * @param repository This is the repository containing the Status objects.
     */
    @Autowired
    public StatusServiceImpl(StatusRepository repository) {
        this.repository = repository;
    }

    /**
     * This method is used to retrieve a Status object from the repository
     * by the given name field of the object.
     *
     * @param name This is the name of the Status object
     * @return Status This method returns the Status object defined by the name parameter.
     */
    @Override
    @Transactional(readOnly = true)
    public Status getByName(String name) {
        if (!repository.existsByName(name)) {
            throw new EntityNotFoundException("Status", name);
        }
        return repository.findByName(name);
    }
}
