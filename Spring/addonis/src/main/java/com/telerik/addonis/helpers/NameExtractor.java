package com.telerik.addonis.helpers;

import com.telerik.addonis.models.Tag;
import com.telerik.addonis.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class NameExtractor {
    private final TagService tagService;

    @Autowired
    public NameExtractor(TagService tagService) {
        this.tagService = tagService;
    }

    public List<String> fromTags(List<Tag> tags) {
        return tagService.getAll()
                .stream()
                .map(Tag::getName)
                .collect(Collectors.toList());
    }
}
