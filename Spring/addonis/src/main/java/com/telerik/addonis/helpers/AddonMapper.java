package com.telerik.addonis.helpers;

import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.Content;
import com.telerik.addonis.models.Status;
import com.telerik.addonis.models.Tag;
import com.telerik.addonis.models.dto.AddonDTO;
import com.telerik.addonis.services.contracts.ContentService;
import com.telerik.addonis.services.contracts.StatusService;
import com.telerik.addonis.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static com.telerik.addonis.constants.AddonStatus.PENDING;

@Component
public class AddonMapper {
    private final StatusService statusService;
    private final SecurityUserHelper currentUser;
    private final ContentService contentService;
    private final FileFactory fileFactory;
    private final TagService tagService;

    @Autowired
    public AddonMapper(StatusService statusService,
                       SecurityUserHelper currentUser,
                       ContentService contentService,
                       FileFactory fileFactory,
                       TagService tagService) {
        this.statusService = statusService;
        this.currentUser = currentUser;
        this.contentService = contentService;
        this.fileFactory = fileFactory;
        this.tagService = tagService;
    }

    public Addon DTOToAddon(AddonDTO dto, MultipartFile file) throws IOException {
        Addon addon = new Addon();
        if (file != null) {
            addon.setContent(addFileToDatabase(dto.getName(), file));
        }
        addon.setName(dto.getName());
        addon.setDescription(dto.getDescription());
        addon.setOrigin(dto.getOrigin());
        addon.setCreator(currentUser.getCurrentUserName());
        addon.setUploadDate(new Date());
        addon.setNumberOfDownloads(0);
        Set<Status> status = new HashSet<>();
        status.add(statusService.getByName(PENDING));
        addon.setStatuses(status);
        addTagsToAddon(dto, addon);
        return addon;
    }

    public AddonDTO AddonToDTO(Addon addon) {
        AddonDTO dto = new AddonDTO();
        dto.setName(addon.getName());
        dto.setDescription(addon.getDescription());
        dto.setOrigin(addon.getOrigin());
        addTagsToDTO(dto, addon.getTags());
        return dto;
    }

    public Addon updateAddon(Addon originalAddon, AddonDTO addon, MultipartFile file) throws IOException {
        if (!addon.getName().isEmpty()) {
            originalAddon.setName(addon.getName());
        }
        if (!addon.getDescription().isEmpty()) {
            originalAddon.setDescription(addon.getDescription());
        }
        if (!addon.getOrigin().isEmpty()) {
            originalAddon.setOrigin(addon.getOrigin());
        }
        if (file != null) {
            originalAddon.setContent(addFileToDatabase(addon.getName(), file));
        }
        addTagsToAddon(addon, originalAddon);
        return originalAddon;
    }

    /**
     * This method is used to create a Content object with the Multipart file given.
     *
     * @param name This is name of the file being stored.
     * @param file This is the multipart file containing data being stored.
     * @return This returns a Set of Content objects containing the newly created Content.
     * @throws IOException when the transfer from Multipart file to byte array was unsuccessful.
     */
    private Set<Content> addFileToDatabase(String name, MultipartFile file) throws IOException {
        Set<Content> contentTable = new HashSet<>();
        Content content = new Content();
        String[] type = file.getOriginalFilename().split("\\.");
        content.setName(name);
        content.setType("." + type[type.length - 1]);
        content.setFile(fileFactory.convertFileToByte(file));
        contentService.create(content);
        contentTable.add(content);
        return contentTable;
    }

    private void addTagsToAddon(AddonDTO dto, Addon addon) {
        if (!dto.getTags().isEmpty()) {
            Set<Tag> tags = Arrays
                    .stream(dto.getTags().split(","))
                    .map(tagService::getByName)
                    .collect(Collectors.toSet());
            addon.setTags(tags);
        }
    }

    private void addTagsToDTO(AddonDTO dto, Set<Tag> tags) {
        if (!tags.isEmpty()) {
            String tagsString = tags
                    .stream()
                    .map(Tag::getName)
                    .collect(Collectors.joining(","));
            dto.setTags(tagsString);
        }
    }
}
