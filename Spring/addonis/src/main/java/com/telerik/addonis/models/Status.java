package com.telerik.addonis.models;

import javax.persistence.*;

/**
 * <h1>Status</h1>
 * Status class represents Statuses in the database.
 * <p>
 * <b>Note:</b> Status is an Entity.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Entity
@Table(name = "statuses")
public class Status {
    /**
     * This is the id of the entity class, which is an auto generated value.
     * The column which corresponds to this field is "id" in the "statuses" table.
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    /**
     * This is a String field, representing the name of the Status.
     * The column which corresponds to this field is "name" in the "statuses" table.
     */
    @Column(name = "name")
    private String name;

    /**
     * This is a default constructor for Status objects.
     */
    public Status() {
    }

    /**
     * This is the getter for the id field.
     *
     * @return This returns the id of the Status object.
     */
    public int getId() {
        return id;
    }

    /**
     * This is the setter for the id field.
     *
     * @param id This is an int, which is going to be the id of the Status object.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * This is the getter for the name field.
     *
     * @return This returns the name of the Status object.
     */
    public String getName() {
        return name;
    }

    /**
     * This is the setter for the name field.
     *
     * @param status This is a String, which is going to be the name of the Status object.
     */
    public void setName(String status) {
        this.name = status;
    }
}
