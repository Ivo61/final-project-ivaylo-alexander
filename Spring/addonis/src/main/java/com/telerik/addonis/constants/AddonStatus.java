package com.telerik.addonis.constants;

/**
 * <h1>AddonStatus</h1>
 * AddonStatus is used to provide a string of constants for every status in our database.
 *
 * @author Ivaylo Staykov, Alexander Stoychev
 * @version 1.0
 * @since 2020-04-15
 */
public interface AddonStatus {
    String FEATURED = "Featured";
    String APPROVED = "Approved";
    String DELETED = "Deleted";
    String PENDING = "Pending";
    String REJECTED = "Rejected";
}
