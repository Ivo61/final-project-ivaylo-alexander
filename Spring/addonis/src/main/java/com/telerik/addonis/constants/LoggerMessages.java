package com.telerik.addonis.constants;

/**
 * <h1>LoggerMessages</h1>
 * LoggerMessages is used to provide a string of constants for logger messages.
 *
 * @author Ivaylo Staykov, Alexander Stoychev
 * @version 1.0
 * @since 2020-04-15
 */
public interface LoggerMessages {
    String USER_ENTERED = " User entered %s page. Endpoint: \"%s\"";
    String ENTITY_NOT_FOUND = " User tried to %s %s with name %s, but it does not exists.";
    String DUPLICATE_ENTITY = " User tried to %s %s with name %s, but it already exists.";
    String FILE_TRANSFER_ERROR = "File transfer was unsuccessful for %s.";
}
