package com.telerik.addonis.helpers;

import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.Rating;
import com.telerik.addonis.models.vo.AddonWithRatingVO;
import com.telerik.addonis.services.contracts.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <h1>AddonExtractor</h1>
 * AddonExtractor is used retrieve Addon objects from lists of other objects.
 * <p>
 * <b>Note:</b> CurrentUser is a component.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Component
public class AddonHelper {
    private final RatingService ratingService;

    @Autowired
    public AddonHelper(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    /**
     * This method returns a sorted list of Addon objects with an average rating above or equal to a given value.
     *
     * @param addons This is the list of Addons to be sorted.
     * @param rating This is the value above which the average rating of the Addon must either be or be above.
     * @return A list of Addon objects, sorted by their average rating;
     */
    public List<Addon> sortByAverageRatingAbove(List<Addon> addons, Double rating) {
        List<Addon> result;
        List<AddonWithRatingVO> ratedAddons = new ArrayList<>();
        for (Addon addon : addons) {
            ratedAddons.add(new AddonWithRatingVO(addon, ratingService.getAverageRatingForAddon(addon.getName())));
        }
        result = ratedAddons
                .stream()
                .sorted((a1, a2) -> a2.getAverageRating().compareTo(a1.getAverageRating()))
                .filter(a -> a.getAverageRating() >= rating)
                .map((a) -> a.getAddon())
                .collect(Collectors.toList());
        return result;
    }
}
