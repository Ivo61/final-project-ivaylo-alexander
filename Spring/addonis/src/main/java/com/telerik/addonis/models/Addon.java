package com.telerik.addonis.models;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * <h1>Addon</h1>
 * Addon class represents addon in the database.
 * <p>
 * <b>Note:</b> Addon is an Entity.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Entity
@Table(name = "addons")
public class Addon {

    /**
     * This is the id of the entity class, which is an auto generated value.
     * The column which corresponds to this field is "id" in the "addons" table.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * This is a String field, representing the name of the Addon.
     * The column which corresponds to this field is "name" in the "addons" table.
     */
    @Column(name = "name")
    private String name;

    /**
     * This is a String field, representing the description of the Addon.
     * The column which corresponds to this field is "description" in the "addons" table.
     */
    @Column(name = "description")
    private String description;

    /**
     * This is a Set of foreign keys from the table "content", the contents of the Set represents
     * objects of the Content class.
     * <p>
     * <b>Note:</b> This has ManyToMany relationship.
     * <p>
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "addon_content",
            joinColumns = {@JoinColumn(name = "addon_id")},
            inverseJoinColumns = {@JoinColumn(name = "content_id")}
    )
    private Set<Content> content;

    /**
     * This is a String field, containing the url to the gitHub repository of the Addon.
     * The column which corresponds to this field is "origin" in the "addons" table.
     */
    @Column(name = "origin")
    private String origin;

    /**
     * This is an int field, containing the number of times the Addon has been downloaded from our database.
     * The column which corresponds to this field is "number_of_downloads" in the "addons" table.
     */
    @Column(name = "number_of_downloads")
    private Integer numberOfDownloads;

    /**
     * This is a Date field, containing the date when the Addon was uploaded to our database.
     * The column which corresponds to this field is "upload_date" in the "addons" table.
     */
    @Column(name = "upload_date")
    private Date uploadDate;

    /**
     * This is a String field, containing username of the User that uploaded the Addon to our database.
     * The column which corresponds to this field is "creator" in the "addons" table.
     */
    @Column(name = "creator")
    private String creator;

    /**
     * This is a Set of foreign keys from the table "statuses", the contents of the Set represents
     * objects of the Status class.
     * <p>
     * <b>Note:</b> This has ManyToMany relationship.
     * <p>
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "addon_status",
            joinColumns = {@JoinColumn(name = "addon_id")},
            inverseJoinColumns = {@JoinColumn(name = "status_id")}
    )
    private Set<Status> statuses;

    /**
     * This is a Set of foreign keys from the table "tags", the contents of the Set represents
     * objects of the Tag class.
     * <p>
     * <b>Note:</b> This has ManyToMany relationship.
     * <p>
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "addon_tags",
            joinColumns = {@JoinColumn(name = "addon_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")}
    )
    private Set<Tag> tags;

    /**
     * This is a default constructor for Addon objects.
     */
    public Addon() {
    }

    /**
     * This is a constructor for Addon objects that copies another Addon object's fields.
     *
     * @param oldAddon is the Addon object being copied.
     */
    public Addon(Addon oldAddon) {
        this.setId(oldAddon.getId());
        this.setName(oldAddon.getName());
        this.setDescription(oldAddon.getDescription());
        this.setOrigin(oldAddon.getOrigin());
        this.setNumberOfDownloads(oldAddon.getNumberOfDownloads());
        this.setUploadDate(oldAddon.getUploadDate());
        this.setCreator(oldAddon.getCreator());
        this.setStatuses(oldAddon.getStatuses());
        this.setContent(oldAddon.getContent());
        this.setTags(oldAddon.getTags());
    }

    /**
     * This is the getter for the id field.
     *
     * @return This returns the id of the Addon object.
     */
    public int getId() {
        return id;
    }

    /**
     * This is the setter for the id field.
     *
     * @param id This is an int, which is going to be the id of the object Addon.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * This is the getter for the name field.
     *
     * @return This returns the name of the Addon object.
     */
    public String getName() {
        return name;
    }

    /**
     * This is the setter for the name field.
     *
     * @param name This is a String, which is going to be the name of the object Addon.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This is the getter for the description field.
     *
     * @return This returns the description of the Addon object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * This is the setter for the description field.
     *
     * @param description This is a String, which is going to be the description of the object Addon.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * This is the getter for the origin field.
     *
     * @return This returns the origin of the Addon object.
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * This is the setter for the origin field.
     *
     * @param origin This is a String, which is going to be the origin of the object Addon.
     */
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    /**
     * This is the getter for the numberOfDownloads field.
     *
     * @return This returns the numberOfDownloads of the Addon object.
     */
    public Integer getNumberOfDownloads() {
        return numberOfDownloads;
    }

    /**
     * This is the setter for the number_of_downloads field.
     *
     * @param number_of_downloads This is an int, which is going to be the number_of_downloads of the object Addon.
     */
    public void setNumberOfDownloads(int number_of_downloads) {
        this.numberOfDownloads = number_of_downloads;
    }

    /**
     * This is the getter for the uploadDate field.
     *
     * @return This returns the uploadDate of the Addon object.
     */
    public Date getUploadDate() {
        return uploadDate;
    }

    /**
     * This is the setter for the upload_date field.
     *
     * @param upload_date This is a Date, which is going to be the upload_date of the object Addon.
     */
    public void setUploadDate(Date upload_date) {
        this.uploadDate = upload_date;
    }

    /**
     * This is the getter for the creator field.
     *
     * @return This returns the creator of the Addon object.
     */
    public String getCreator() {
        return creator;
    }

    /**
     * This is the setter for the creator field.
     *
     * @param creator This is a String, which is going to be the creator of the object Addon.
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * This is the getter for the status field.
     *
     * @return This returns the status of the Addon object.
     */
    public Set<Status> getStatuses() {
        return statuses;
    }

    /**
     * This is the setter for the status field.
     *
     * @param statuses This is a Set of Status objects, which is going to be the status of the object Addon.
     */
    public void setStatuses(Set<Status> statuses) {
        this.statuses = statuses;
    }

    /**
     * This is the getter for the tags field.
     *
     * @return This returns the tags of the Addon object.
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * This is the setter for the tags field.
     *
     * @param tags This is a Ste of Tag objects, which is going to be the tags of the object Addon.
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * This is the getter for the content field.
     *
     * @return This returns the content of the Addon object.
     */
    public Set<Content> getContent() {
        return content;
    }

    /**
     * This is the setter for the content field.
     *
     * @param contentDB This is a Set of Content objects, which is going to be the content of the object Addon.
     */
    public void setContent(Set<Content> contentDB) {
        this.content = contentDB;
    }
}
