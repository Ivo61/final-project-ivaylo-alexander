package com.telerik.addonis.exceptions;

/**
 * <h1>EntityNotFoundException</h1>
 * EntityNotFoundException is a custom exception class, which is used when the entity we are looking
 * for does not exist.
 * <p>
 * <b>Note:</b> EntityNotFoundException extends the RuntimeException.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
public class EntityNotFoundException extends RuntimeException {

    /**
     * This method is to throw an exception.
     *
     * @param typeOfObject This is the name of the object as a string to be used for the exception
     *                     message.
     * @param object       This is the specific object as string that was provided, but not found,
     *                     used for the eception message
     * @throws EntityNotFoundException, which is a Runtime exception
     */
    public EntityNotFoundException(String typeOfObject, String object) {
        super(String.format("The following %s could not be found: %s", typeOfObject, object));
    }

    /**
     * This method is to throw an exception.
     *
     * @param typeOfObject This is the name of the object as a string to be used for the exception
     *                     message.
     * @throws EntityNotFoundException, which is a Runtime exception.
     */
    public EntityNotFoundException(String typeOfObject) {
        super(String.format("This %s could not be found", typeOfObject));
    }

}
