package com.telerik.addonis.repositories;

import com.telerik.addonis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <h1>UserRepository</h1>
 * UserRepository is used to access our database.
 * <p>
 * <b>Note:</b> UserRepository extends the JpaRepository interface.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {
    /**
     * This method is used to check if a specific User exists.
     *
     * @param username This is the name of a User object
     * @return This tells us if the User exists.
     */
    boolean existsByUsername(String username);

    /**
     * This method is used to retrieve a User object from the repository
     * by the given username field of the object.
     *
     * @param username This is the username of the User object
     * @return This method returns the User object.
     */
    User findByUsername(String username);

}
