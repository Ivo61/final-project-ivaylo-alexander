package com.telerik.addonis.controllers.rest;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Tag;
import com.telerik.addonis.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * <h1>TagRestController</h1>
 * TagRestController is a REST controller that provides CRUD operations for Tag
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@RestController
@RequestMapping("/api/tags")
public class TagRestController {
    /**
     * The service that has access to the database.
     */
    private final TagService service;

    /**
     * This is the constructor for TagRestController objects.
     *
     * @param service This is the service containing the the logic behind the CRUD operations.
     */
    @Autowired
    public TagRestController(TagService service) {
        this.service = service;
    }

    /**
     * This method is used to get all Tag objects in our database as a List
     *
     * @return List of the Object Tag
     */
    @GetMapping
    public List<Tag> getAll() {
        return service.getAll();
    }

    /**
     * This method is used to get one Tag object by name.
     *
     * @param tagName This is a string, which is a field in the object Tag
     * @return This method returns an object of type Tag.
     */
    @GetMapping("/{tagName}")
    public Tag getByName(@PathVariable(name = "tagName") String tagName) {
        try {
            return service.getByName(tagName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * This method is used to create an object of type Tag.
     *
     * @param tag This is an object that has: int id, string name.
     * @return This returns an object of type Tag.
     */
    @PostMapping
    public Tag create(@RequestBody Tag tag) {
        try {
            return service.create(tag);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    /**
     * This method is used to update an existing Tag.
     *
     * @param id  This is the id of the tag which, we want to update.
     * @param tag This is the value we want to change.
     * @return This returns an object of type Tag.
     */
    @PutMapping("/{id}")
    public Tag update(@PathVariable("id") int id, @RequestBody Tag tag) {
        try {
            return service.update(tag);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * This method is used to delete an existing Addon
     *
     * @param tagName This is a string that corresponds to a Tag with the same name
     *                in our database.
     * @return This returns an object of type Tag.
     */
    @DeleteMapping("/{tagName}")
    public Tag delete(@PathVariable(name = "tagName") String tagName) {
        try {
            return service.delete(tagName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
