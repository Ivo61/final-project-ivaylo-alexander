package com.telerik.addonis.constants;

/**
 * <h1>TimeIntervals</h1>
 * TimeIntervals is used to provide values in milliseconds for different time intervals used to discern which
 * Addons were created recently.
 *
 * @author Ivaylo Staykov, Alexander Stoychev
 * @version 1.0
 * @since 2020-04-15
 */
public interface TimeIntervals {
    long EIGHT_HOURS = 1000 * 60 * 60 * 8;
    long DAY = 1000 * 60 * 60 * 24;
    long WEEK = 1000 * 60 * 60 * 24 * 7;
}
