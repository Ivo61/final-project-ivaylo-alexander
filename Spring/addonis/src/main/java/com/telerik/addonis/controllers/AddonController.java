package com.telerik.addonis.controllers;

import com.telerik.addonis.constants.AddonStatus;
import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.helpers.*;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.dto.AddonDTO;
import com.telerik.addonis.repositories.GitHubApiRepositoryImpl;
import com.telerik.addonis.services.contracts.AddonService;
import com.telerik.addonis.services.contracts.RatingService;
import com.telerik.addonis.services.contracts.TagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import static com.telerik.addonis.constants.ButtonType.*;
import static com.telerik.addonis.constants.LoggerMessages.*;
import static com.telerik.addonis.constants.PageSizes.DEFAULT_CARD_LIMIT;
import static com.telerik.addonis.constants.PageSizes.DEFAULT_PAGE_LIMIT;
import static com.telerik.addonis.constants.TimeIntervals.WEEK;

/**
 * <h1>AddonController</h1>
 * StatusServiceImpl is an MVC Controller that provides us with our front-end logic and templates.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Controller
public class AddonController {
    /**
     * These are all the @Components and @Services we need in order to implement our logic.
     */
    private final Logger logger = LoggerFactory.getLogger(AddonController.class);
    private final AddonService service;
    private final TagService tagService;
    private final RatingService ratingService;
    private final GitHubApiRepositoryImpl gitHubApiRepositoryImpl;
    private final NameExtractor nameExtractor;
    private final AddonMapper mapper;
    private final SecurityUserHelper helper;
    private final AddonHelper addonHelper;
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd");

    @Autowired
    public AddonController(AddonService service,
                           TagService tagService,
                           NameExtractor nameExtractor,
                           RatingService ratingService,
                           GitHubApiRepositoryImpl gitHubApiRepositoryImpl,
                           AddonMapper mapper,
                           SecurityUserHelper helper,
                           AddonHelper addonHelper) {
        this.service = service;
        this.tagService = tagService;
        this.ratingService = ratingService;
        this.nameExtractor = nameExtractor;
        this.gitHubApiRepositoryImpl = gitHubApiRepositoryImpl;
        this.mapper = mapper;
        this.helper = helper;
        this.addonHelper = addonHelper;

    }

    /**
     * This method provides index template with attributes -to be utilized to display a homepage with three
     * distinct tables. First table has 5 Addons with Status Featured.
     * <p>
     * <b>Note:</b> The url for this is: /
     * <p>
     *
     * @param model This is used to add attributes to the template.
     * @return index - This is the template to which the attributes must go.
     */
    @GetMapping("/")
    public String showHomePage(Model model) {
        logger.info(String.format(helper.getCurrentUserName()+USER_ENTERED, "home", "/"));
        PaginationHelper.resetSort();
        List<Addon> approvedAddons = service.filterByStatus(service.getAll(), AddonStatus.APPROVED);
        List<Addon> featuredAddons = service.filterByStatus(approvedAddons, AddonStatus.FEATURED);
        List<Addon> newAddons = service.filterByCreationDate(approvedAddons, WEEK);
        List<Addon> ratedAddons = addonHelper.sortByAverageRatingAbove(approvedAddons, 3.0);
        List<Addon> popularAddons = service.sortByDownloadCount(approvedAddons);
        model.addAttribute("addons", service.getAll());
        model.addAttribute("featured", featuredAddons
                .stream().limit(DEFAULT_CARD_LIMIT).collect(Collectors.toList()));
        model.addAttribute("newlyMade", newAddons
                .stream().limit(DEFAULT_CARD_LIMIT).collect(Collectors.toList()));
        model.addAttribute("highlyRated", ratedAddons
                .stream().limit(DEFAULT_CARD_LIMIT).collect(Collectors.toList()));
        model.addAttribute("popular", popularAddons
                .stream().limit(DEFAULT_CARD_LIMIT).collect(Collectors.toList()));
        model.addAttribute("formatter", dateFormatter);
        model.addAttribute("github", gitHubApiRepositoryImpl);
        model.addAttribute("username", helper.getCurrentUserName());
        return "index";
    }

    /**
     * This method provides a table with all addons in our application with Status: Approved. It also adds a specific
     * format to the dates, which we show.
     * <p>
     * <b>Note:</b> The url for this is: /addons
     * <p>
     *
     * @param model       This is used to add attributes to the template.
     * @param currentPage This is the current page, it can change depending on the result of changePage();
     * @return addons - This is the template to which the attributes must go.
     */
    @GetMapping("/addons")
    public String showAddons(Model model, Integer currentPage) {
        if (currentPage == null) {
            currentPage = 0;
        }
        List<Addon> approvedAddons = service.filterByStatus(service.getAll(), AddonStatus.APPROVED);
        Pageable page = PageRequest.of(currentPage, DEFAULT_PAGE_LIMIT);
        model.addAttribute("github", gitHubApiRepositoryImpl);
        model.addAttribute("maxPages", approvedAddons.size() / 5);
        model.addAttribute("addons", service.paginationWithSortByStatus(PaginationHelper.sort, PaginationHelper.order, page));
        model.addAttribute("formatter", dateFormatter);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("selectedSort", PaginationHelper.sort);
        model.addAttribute("username", helper.getCurrentUserName());
        return "addons";
    }

    @PostMapping("/addons")
    public String changePage(@RequestParam(value = "next") String next, Model model) {
        return showAddons(model, Integer.parseInt(next));
    }

    @PostMapping("/sort")
    public String sort(@RequestParam(value = "sortBy") String sortBy) {
        if (PaginationHelper.sort.equalsIgnoreCase(sortBy)) {
            PaginationHelper.order = 1;
        } else {
            PaginationHelper.order = 0;
        }
        PaginationHelper.sort = sortBy;
        return "redirect:/addons";
    }

    @GetMapping("/addon/view/{name}")
    public String showAddon(@PathVariable String name, Model model) throws IOException {
        PaginationHelper.resetSort();
        Addon addon = service.getByName(name);
        if (!addon.getOrigin().isEmpty()) {
            model.addAttribute("totalPullRequests", gitHubApiRepositoryImpl.getPullRequestsCount(addon.getOrigin()));
            model.addAttribute("totalOpenIssues", gitHubApiRepositoryImpl.getOpenIssueCount(addon.getOrigin()));
            model.addAttribute("lastCommitDate", gitHubApiRepositoryImpl.getLastCommitDate(addon.getOrigin()));
            model.addAttribute("lastCommitTitle", gitHubApiRepositoryImpl.getLastCommitTitle(addon.getOrigin()));
        }
        model.addAttribute("addon", addon);
        model.addAttribute("averageRating", String.format("%.2f", ratingService.getAverageRatingForAddon(name)));
        model.addAttribute("statuses", service.getStatusNames(name));
        model.addAttribute("isAdmin", helper.isUserAdmin(helper.getCurrentUserName()));
        model.addAttribute("username", helper.getCurrentUserName());
        return "addon-view";
    }

    @GetMapping("/addon/create")
    public String showCreateFrom(Model model) {
        logger.info(String.format(helper.getCurrentUserName()+USER_ENTERED, "create addon", "/addon/create"));
        PaginationHelper.resetSort();
        model.addAttribute("addon", new AddonDTO());
        model.addAttribute("tags", nameExtractor.fromTags(tagService.getAll()));
        model.addAttribute("username", helper.getCurrentUserName());
        return "addon";
    }

    @PostMapping("addon/create")
    public String createAddon(@Valid @ModelAttribute("addon") AddonDTO addon,
                              BindingResult errors,
                              @RequestParam("file") MultipartFile file,
                              Model model) {
        if (errors.hasErrors()) {
            return "addon";
        }
        try {
            service.create(mapper.DTOToAddon(addon, file));
        } catch (IOException ex) {
            model.addAttribute("error", ex.getMessage());
            logger.error(String.format(FILE_TRANSFER_ERROR, addon.getName()));
            return "error";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            logger.error(String.format(helper.getCurrentUserName()+DUPLICATE_ENTITY, "create", "addon", addon.getName()));
            return "addon";
        }
        return "redirect:/";
    }


    @GetMapping("/addon/edit/{name}")
    public String showEditForm(@PathVariable String name, Model model) {
        logger.info(String.format(helper.getCurrentUserName()+USER_ENTERED, "addon edit", "/addon/edit"));
        PaginationHelper.resetSort();
        model.addAttribute("allTags", nameExtractor.fromTags(tagService.getAll()));
        model.addAttribute("specificTags", service.getTagNames(name));
        model.addAttribute("id", service.getByName(name).getId());
        model.addAttribute("addon", mapper.AddonToDTO(service.getByName(name)));
        model.addAttribute("username", helper.getCurrentUserName());
        return "addon-edit";
    }


    @RequestMapping(value = "/addon/edit/{name}/update")
    public String edit(@Valid @ModelAttribute("addon") AddonDTO dto,
                       @RequestParam("action") String buttonType,
                       @RequestParam("file") MultipartFile file,
                       @RequestParam("id") String id,
                       BindingResult errors,
                       Model model) {
        if (errors.hasErrors() && !buttonType.equals("Index")) {
            return "addon-edit";
        }
        switch (buttonType) {
            case UPDATE:
                try {
                    service.update(Integer.parseInt(id), mapper.updateAddon(service.getById(Integer.parseInt(id)), dto, file));
                    return "redirect:/addon/view/" + dto.getName();
                } catch (EntityNotFoundException e) {
                    model.addAttribute("error", e.getMessage());
                    logger.error(String.format(helper.getCurrentUserName()+ENTITY_NOT_FOUND, "edit", "addon", dto.getName()));
                    return "addon-edit";
                } catch (DuplicateEntityException e) {
                    model.addAttribute("error", e.getMessage());
                    logger.error(String.format(helper.getCurrentUserName()+DUPLICATE_ENTITY, "edit", "addon", dto.getName()));
                    return "addon-edit";
                } catch (IOException ex) {
                    model.addAttribute("error", ex.getMessage());
                    logger.error(String.format(FILE_TRANSFER_ERROR, dto.getName()));
                    return "error";
                }
            case DELETE:
                try {
                    service.delete(dto.getName());
                    return "redirect:/addon/view/" + dto.getName();
                } catch (EntityNotFoundException e) {
                    model.addAttribute("error", e.getMessage());
                    logger.error(String.format(helper.getCurrentUserName()+ENTITY_NOT_FOUND, "delete", "addon", dto.getName()));
                    return "addon-edit";
                }
            case INDEX:
                return "redirect:/";
            default:
                return "redirect:/addon/view/" + dto.getName();
        }
    }

    @RequestMapping(value = "/addon/view/rate")
    public String rate(@RequestParam(name = "addon") String addon, @RequestParam(name = "score") String score) {
        try {
            ratingService.rate(addon, score);
        } catch (EntityNotFoundException e) {
            logger.error(String.format(helper.getCurrentUserName()+ENTITY_NOT_FOUND, "rate", "addon", addon));
            return "error";
        }
        return "redirect:/addon/view/" + addon;
    }

    @RequestMapping(value = "/addon/view/approve")
    public String approve(@RequestParam(name = "addon") String addon, Model model) {
        try {
            service.approve(addon);
            return "redirect:/addon/view/" + addon;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            logger.error(String.format(helper.getCurrentUserName()+ENTITY_NOT_FOUND, "approve", "addon", addon));
            return "redirect:/addon/view/" + addon;
        }
    }

    @RequestMapping(value = "/addon/view/reject")
    public String reject(@RequestParam(name = "addon") String addon, Model model) {
        try {
            service.reject(addon);
            return "redirect:/addon/view/" + addon;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            logger.error(String.format(helper.getCurrentUserName()+ENTITY_NOT_FOUND, "reject", "addon", addon));
            return "redirect:/addon/view/" + addon;
        }
    }

    @RequestMapping(value = "/addon/view/feature")
    public String feature(@RequestParam(name = "addon") String addon, Model model) {
        try {
            service.toggleFeatured(addon);
            return "redirect:/addon/view/" + addon;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            logger.error(String.format(helper.getCurrentUserName()+ENTITY_NOT_FOUND, "feature", "addon", addon));
            return "redirect:/addon/view/" + addon;
        }
    }

    @RequestMapping(value = "/addon/view/edit")
    public String edit(@RequestParam(name = "addon") String addon) {
        return "redirect:/addon/edit/" + addon;
    }


    @RequestMapping(value = "/addon/filter", method = RequestMethod.POST)
    public String filterAddon(@RequestParam(value = "keyword") String keyword,
                              @RequestParam(value = "whereToLook") String whereToLook,
                              Model model) {
        PaginationHelper.resetSort();
        model.addAttribute("addons", service.filter(keyword, whereToLook));
        model.addAttribute("keyword", keyword);
        model.addAttribute("github", gitHubApiRepositoryImpl);
        model.addAttribute("formatter", dateFormatter);
        model.addAttribute("username", helper.getCurrentUserName());
        return "filter";
    }

}
