package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.Status;

/**
 * <h1>StatusService</h1>
 * StatusService is used to give the logic of our application regarding statuses.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
public interface StatusService {
    /**
     * This method is used to retrieve a specific Status by its name.
     *
     * @param name This is the name of the Status object
     * @return This returns a Status object.
     */
    Status getByName(String name);
}
