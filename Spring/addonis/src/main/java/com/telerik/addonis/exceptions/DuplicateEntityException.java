package com.telerik.addonis.exceptions;

/**
 * <h1>DuplicateEntityException</h1>
 * DuplicateEntityException is a custom exception class that is thrown when an object already exists
 * in the database.
 * <p>
 * <b>Note:</b> DuplicateEntityException extends RuntimeException.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
public class DuplicateEntityException extends RuntimeException {

    /**
     * This is the constructor for DuplicateEntityException objects.
     *
     * @param typeOfObject This is a String describing the class of the object.
     * @param object       This is a String describing the name of the object.
     */
    public DuplicateEntityException(String typeOfObject, String object) {
        super(String.format("The following %s already exists: %s", typeOfObject, object));
    }
}