package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Tag;
import com.telerik.addonis.repositories.TagRepository;
import com.telerik.addonis.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <h1>TagServiceImpl</h1>
 * TagServiceImpl is used to give access to the database when dealing with objects of the Tag Class,
 * it also provides the logic.
 * <p>
 * <b>Note:</b> TagServiceImpl implements the TagService interface.
 * <b>Note:</b> This is a @Service.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Service
public class TagServiceImpl implements TagService {
    /**
     * These are all the @Components and @Services we need in order to implement our logic.
     */
    private final TagRepository repository;

    /**
     * This is the constructor for the TagServiceImpl objects, generating all the @Components and @Services needed.
     */
    @Autowired
    public TagServiceImpl(TagRepository repository) {
        this.repository = repository;
    }

    /**
     * This method is used to retrieve all Tags in our database.
     *
     * @return This returns all of the Tag objects in our database as a list.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Tag> getAll() {
        return repository.findAll();
    }

    /**
     * This method is used to retrieve a specific Tag by its name field.
     *
     * @param tagName This is the name of the Tag object being searched for.
     * @return This returns the Tag object corresponding to the name given.
     */
    @Override
    @Transactional(readOnly = true)
    public Tag getByName(String tagName) {
        throwIfTagDoesNotExist(tagName);
        return repository.findByName(tagName);
    }

    /**
     * This method is used to create a new Tag object.
     *
     * @param tag This is a Tag object containing the data for the fields of the Tag being created.
     * @return This returns the created Tag object.
     */
    @Override
    public Tag create(Tag tag) {
        if (repository.existsByName(tag.getName())) {
            throw new DuplicateEntityException("Tag", tag.getName());
        }
        return repository.save(tag);
    }

    /**
     * This method is used to update an existing Tag object.
     *
     * @param tag This is the Tag object that is to be updated.
     * @return This returns the edited Tag object.
     */
    @Override
    public Tag update(Tag tag) {
        throwIfTagDoesNotExist(tag.getName());
        return repository.save(tag);
    }

    /**
     * This method is used to delete an existing Tag object by its name field.
     *
     * @param tagName This is the name of the Tag object.
     * @return This returns the deleted Tag object.
     */
    @Override
    public Tag delete(String tagName) {
        throwIfTagDoesNotExist(tagName);
        Tag deletedTag = getByName(tagName);
        repository.deleteByName(tagName);
        return deletedTag;
    }

    private void throwIfTagDoesNotExist(String tagName) {
        if (!repository.existsByName(tagName)) {
            throw new EntityNotFoundException("Tag", tagName);
        }
    }
}
