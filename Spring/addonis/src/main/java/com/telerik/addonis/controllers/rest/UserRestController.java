package com.telerik.addonis.controllers.rest;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.User;
import com.telerik.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * <h1>UserRestController</h1>
 * UserRestController is a REST controller that provides CRUD operations for User objects
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@RestController
@RequestMapping("/api/users")
public class UserRestController {
    /**
     * The service that has access to the database.
     */
    private final UserService service;

    /**
     * This is the constructor for TagRestController objects.
     *
     * @param service This is the service containing the the logic behind the CRUD operations.
     */
    @Autowired
    public UserRestController(UserService service) {
        this.service = service;
    }

    /**
     * This method is used to get all User objects in our database as a List
     *
     * @return List of the User objects
     */
    @GetMapping
    public List<User> getAll() {
        return service.getAll();
    }

    /**
     * This method is used to create an object of type User.
     *
     * @param user This is an object that has: String username, String password, String email, byte enabled,
     *             String passwordConfirmation.
     * @return This returns the User object that was created.
     */
    @PostMapping
    public User register(@RequestBody User user) {
        try {
            return service.register(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    /**
     * This method is used to update an existing User.
     *
     * @param user This is the object we want to change.
     * @return This returns the User object that was edited.
     */
    @PutMapping
    public User update(@RequestBody User user) {
        try {
            return service.update(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * This method is used to disable an existing Tag.
     *
     * @param username This is a string that corresponds to a User with the same name in our database.
     * @return This returns the User object that was disabled.
     */
    @PutMapping("/{username}")
    public User disableUser(@PathVariable(name = "username") String username) {
        try {
            return service.disableUser(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
