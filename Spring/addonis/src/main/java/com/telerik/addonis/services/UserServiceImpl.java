package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.helpers.UserMapper;
import com.telerik.addonis.models.User;
import com.telerik.addonis.repositories.UserRepository;
import com.telerik.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <h1>UserServiceImpl</h1>
 * UserServiceImpl is used to provide the logic for User operations and a connection to the repository.
 * <p>
 * <b>Note:</b> UserServiceImpl implements the UserService interface.
 * <b>Note:</b> This is a @Service.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Service
public class UserServiceImpl implements UserService {
    /**
     * These are all the @Components and @Services we need in order to implement our logic.
     */
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository repository;
    private final UserMapper mapper;

    @Autowired
    public UserServiceImpl(UserDetailsManager userDetailsManager,
                           PasswordEncoder passwordEncoder,
                           UserRepository repository,
                           UserMapper mapper) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.repository = repository;
        this.mapper = mapper;
    }

    /**
     * This method is used to retrieve all Users in our database.
     * <p>
     * <b>Note:</b> This is set as @Transactional(readOnly = true) for faster execution.
     * <p>
     *
     * @return This returns all of the users in our database as a list.
     */
    @Override
    @Transactional(readOnly = true)
    public List<User> getAll() {
        return repository.findAll();
    }

    /**
     * This method is used to retrieve a specific User by its name.
     * <p>
     * <b>Note:</b> This is set as @Transactional(readOnly = true) for faster execution.
     * <p>
     *
     * @param userName This is the name of the User object
     * @return This returns a User object.
     */
    @Override
    @Transactional(readOnly = true)
    public User getByName(String userName) {
        throwIfNotFound(userName);
        return repository.findByUsername(userName);
    }

    /**
     * This method creates a Spring Security user and also saves our User with additional details(email)
     * to our database. It also sets the role of the user as: ROLE_USER and encodes. This method also hashes the
     * password by utilizing the method: passwordEncoder.
     *
     * @param user This is a User object.
     * @return This method returns the User object.
     * @throws DuplicateEntityException if the User we are trying to create exists.
     */
    @Override
    public User register(User user) {
        if (repository.existsByUsername(user.getUsername())) {
            throw new DuplicateEntityException("User", user.getUsername());
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEnabled((byte) 1);
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        user.getPassword(),
                        authorities);
        userDetailsManager.createUser(newUser);
        return repository.save(user);
    }

    /**
     * This method updates an existing User. It check is the provided fields are empty,
     * if they are not empty it saves the changes. If a password is changed it also hashes the password using
     * the a method from Spring security: passwordEncoder.
     *
     * @param user This is a User object.
     * @return This method returns the User object.
     * @throws EntityNotFoundException is the User you are trying to access does not exist.
     */
    @Override
    public User update(User user) {
        throwIfNotFound(user.getUsername());
        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            throw new IllegalArgumentException();
        }
        return repository.save(mapper.updateUser(getByName(user.getUsername()), user));
    }

    /**
     * This method is used to disable a User. Spring Security has a functionally for disabling/enabling users.
     * It forces us to create a column called "enabled", which can be 1 or 0, depending on the value
     * the User can log in or not. In this method we change the value to 0 to disable the user. We use
     * this method as a soft delete for users.
     *
     * @param username This is the name of the User object
     * @return This method returns the User object.
     * @throws EntityNotFoundException is the User you are trying to access does not exist.
     */
    @Override
    public User disableUser(String username) {
        throwIfNotFound(username);
        User disabledUser = repository.findByUsername(username);
        disabledUser.setEnabled((byte) 0);
        return repository.save(disabledUser);
    }

    /**
     * This method is used to enable a User. Spring Security has a functionally for disabling/enabling users.
     * It forces us to create a column called "enabled", which can be 1 or 0, depending on the value
     * the User can log in or not. In this method we change the value to 1 to enable the user.
     *
     * @param username This is the name of the User object
     * @return This method returns the User object.
     * @throws EntityNotFoundException is the User you are trying to access does not exist.
     */
    @Override
    public User enableUser(String username) {
        throwIfNotFound(username);
        User enabledUser = repository.findByUsername(username);
        enabledUser.setEnabled((byte) 1);
        return repository.save(enabledUser);
    }

    private void throwIfNotFound(String username) {
        if (!repository.existsByUsername(username)) {
            throw new EntityNotFoundException("User", username);
        }
    }

}
