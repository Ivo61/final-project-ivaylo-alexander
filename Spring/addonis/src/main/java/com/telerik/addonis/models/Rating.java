package com.telerik.addonis.models;

import javax.persistence.*;

/**
 * <h1>Rating</h1>
 * Rating this class represents Addon_rating in the database.
 * <p>
 * <b>Note:</b> Rating is an Entity.
 * <b>Note:</b> This class implements the Comparable interface.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Entity
@Table(name = "addon_rating")
public class Rating implements Comparable<Rating> {
    /**
     * This is the id of the entity class, which is an auto generated value.
     * The column which corresponds to this field is "id".
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * This is a foreign key from the table "users", the column corresponds to the primary
     * key of that table, which is "username".
     * <p>
     * <b>Note:</b> This has OneToOne relationship.
     * <p>
     */
    @OneToOne
    @JoinColumn(name = "username")
    private User user;

    /**
     * This is a foreign key from the table "addon", the column corresponds to the primary
     * key of that table, which is "addon_id".
     * <p>
     * <b>Note:</b> This has OneToOne relationship.
     * <p>
     */
    @OneToOne
    @JoinColumn(name = "addon_id")
    private Addon addon;

    /**
     * This is an int field, which is the rating given by a specific user for a specific addon.
     * The column which corresponds to this field is "rating".
     */
    @Column(name = "rating")
    private int rating;

    public Rating() {
    }

    /**
     * This is the getter for id.
     *
     * @return This returns the id of the object Rating.
     */
    public int getId() {
        return id;
    }

    /**
     * This is the setter for id.
     *
     * @param id This is a int, which is going to be the id of the object Rating.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * This is the getter for user.
     *
     * @return This returns the user of the object Rating.
     */
    public User getUser() {
        return user;
    }

    /**
     * This is the setter for user.
     *
     * @param user This is an object of type User, which is going to be the user for the object Rating.
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * This is the getter for addon.
     *
     * @return This returns the addon of the object Rating.
     */
    public Addon getAddon() {
        return addon;
    }

    /**
     * This is the setter for ratting
     *
     * @param addon This is an object of type Addon, which is going to be the addon for the object Rating.
     */
    public void setAddon(Addon addon) {
        this.addon = addon;
    }

    /**
     * This is the getter for rating.
     *
     * @return This returns the rating of the object Rating.
     */
    public int getRating() {
        return rating;
    }

    /**
     * This is the setter for rating.
     *
     * @param rating This is an int that is going to be the rating for the object Rating.
     */
    public void setRating(int rating) {
        this.rating = rating;
    }

    /**
     * This is method overrides the compareTo method of the Comparable interface.
     */
    @Override
    public int compareTo(Rating rating) {
        Integer thisInteger = this.getRating();
        Integer parameterInteger = rating.getRating();
        return thisInteger.compareTo(parameterInteger);
    }
}
