package com.telerik.addonis.controllers;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.helpers.PaginationHelper;
import com.telerik.addonis.models.User;
import com.telerik.addonis.services.contracts.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

import static com.telerik.addonis.constants.LoggerMessages.DUPLICATE_ENTITY;
import static com.telerik.addonis.constants.LoggerMessages.USER_ENTERED;

/**
 * <h1>RegisterController</h1>
 * RegisterController is an MVC Controller that provides us with the register page template which facilitates
 * the registration of new users.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Controller
public class RegisterController {
    /**
     * These are all the @Components and @Services we need in order to implement our logic.
     */
    private final Logger logger = LoggerFactory.getLogger(AddonController.class);
    private final UserService service;

    /**
     * This is the constructor for RegisterController objects.
     *
     * @param service This is the service containing the the logic for manipulating Users.
     */
    @Autowired
    public RegisterController(UserService service) {
        this.service = service;
    }

    /**
     * This method provides the register template with attributes -to be utilized to display a registration
     * form
     * <p>
     * <b>Note:</b> The url for this is: /register
     * <p>
     *
     * @param model This is used to add attributes to the template.
     * @return register - This is the template to which the controller redirects.
     */
    @GetMapping("/register")
    public String showRegistrationForm(Model model) {
        PaginationHelper.resetSort();
        logger.info(String.format(USER_ENTERED, "registration", "/register"));
        model.addAttribute("user", new User());
        return "register";
    }

    /**
     * This method is used to create an object of type User.
     *
     * @param user          This is an object that has: String username, String password, String email, byte enabled,
     *                      String passwordConfirmation.
     * @param bindingResult this is an object used to store input errors when using the template
     * @param model         This is used to add attributes to the template.
     * @return register template if errors are present
     * login template if the registration is successful
     */
    @PostMapping("/register")
    @Transactional
    public String registerUser(@Valid @ModelAttribute("user") User user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username, password and email can't be empty.");
            return "register";
        }
        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("error", "Passwords do not match.");
            return "register";
        }
        try {
            service.register(user);
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            logger.error(String.format(DUPLICATE_ENTITY, "register", "user", user.getUsername()));
            return "register";
        }
        return "redirect:login";
    }

}
