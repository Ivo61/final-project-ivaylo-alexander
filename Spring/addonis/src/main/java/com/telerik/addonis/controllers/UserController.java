package com.telerik.addonis.controllers;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.helpers.PaginationHelper;
import com.telerik.addonis.helpers.SecurityUserHelper;
import com.telerik.addonis.models.User;
import com.telerik.addonis.services.contracts.AddonService;
import com.telerik.addonis.services.contracts.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.telerik.addonis.constants.AddonStatus.*;
import static com.telerik.addonis.constants.ButtonType.*;
import static com.telerik.addonis.constants.LoggerMessages.*;

@Controller
public class UserController {
    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final UserService service;
    private final AddonService addonService;
    private final SecurityUserHelper helper;

    @Autowired
    public UserController(UserService service, SecurityUserHelper helper, AddonService addonService) {
        this.service = service;
        this.helper = helper;
        this.addonService = addonService;
    }

    @GetMapping({"/user", "/user/{userName}"})
    public String showUser(Model model, @PathVariable(value = "userName", required = false) String userName) {
        PaginationHelper.resetSort();
        User user;
        if (userName != null) {
            if(!helper.getCurrentUserName().equals(userName) && !helper.isUserAdmin(helper.getCurrentUserName())){
                return "access-denied";
            }
            logger.info(String.format(helper.getCurrentUserName()+USER_ENTERED, "user panel", "/user/" + userName));
            user = service.getByName(userName);
        } else {
            logger.info(String.format(helper.getCurrentUserName()+USER_ENTERED, "user panel", "/user"));
            user = service.getByName(helper.getCurrentUserName());
        }
        User currentUser = user;
        model.addAttribute("user", currentUser);
        model.addAttribute("approved", addonService.getByUserAndStatus(APPROVED, currentUser.getUsername()));
        model.addAttribute("pending", addonService.getByUserAndStatus(PENDING, currentUser.getUsername()));
        model.addAttribute("rejected", addonService.getByUserAndStatus(REJECTED, currentUser.getUsername()));
        model.addAttribute("allPending", addonService.filterByStatus(addonService.getAll(), PENDING));
        model.addAttribute("username", helper.getCurrentUserName());
        return "user-panel";
    }

    @RequestMapping("user")
    public String edit(@ModelAttribute("user") User user, @RequestParam("action") String buttonType, Model model) {
        switch (buttonType) {
            case UPDATE_USER:
                try {
                    if (!user.getPassword().equals(user.getPasswordConfirmation())) {
                        model.addAttribute("error", "Passwords do not match.");
                        return "register";
                    }
                    service.update(user);
                    return "redirect:/user";
                } catch (EntityNotFoundException e) {
                    model.addAttribute("error", e.getMessage());
                    logger.error(String.format(helper.getCurrentUserName()+ENTITY_NOT_FOUND, "edit", "user", user.getUsername()));
                    return "user-panel";
                }
            case DISABLE_USER:
                try {
                    service.disableUser(user.getUsername());
                    return "redirect:/users";
                } catch (EntityNotFoundException e) {
                    model.addAttribute("error", e.getMessage());
                    logger.error(String.format(helper.getCurrentUserName()+ENTITY_NOT_FOUND, "disable", "user", user.getUsername()));
                    return "user-panel";
                }
            case ENABLE_USER:
                try {
                    service.enableUser(user.getUsername());
                    return "redirect:/users";
                } catch (EntityNotFoundException e) {
                    model.addAttribute("error", e.getMessage());
                    logger.error(String.format(helper.getCurrentUserName()+ENTITY_NOT_FOUND, "enable", "user", user.getUsername()));
                    return "user-panel";
                }
            case INDEX:
                return "redirect:/";
            case CREATE_ADDON:
                return "redirect:/addon/create";
            case VIEW_USERS:
                return "redirect:/users";
            case ALL_TAGS:
                return "redirect:/tags";
            default:
                return "redirect:/user";
        }

    }

    @GetMapping("/users")
    public String showAllUsers(Model model) {
        logger.info(String.format(helper.getCurrentUserName()+USER_ENTERED, "all users", "/users"));
        PaginationHelper.resetSort();
        List<User> allUsers = service.getAll();
        model.addAttribute("users", allUsers);
        model.addAttribute("username", helper.getCurrentUserName());
        return "users";
    }

    @RequestMapping("users")
    public String redirect(@RequestParam("action") String buttonType) {
        switch (buttonType) {
            case INDEX:
                return "index";
            case PERSONAL_PANEL:
                return "redirect:user";
            default:
                return "index";
        }
    }
}
