package com.telerik.addonis.controllers;

import com.telerik.addonis.helpers.PaginationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * <h1>LoginController</h1>
 * LoginController is used for our login logic it's an MVC Controller.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Controller
public class LoginController {
    /**
     * This method is used to access the login.html which has the login form.
     * To access the from use the following URL: /login.
     *
     * @return This string is and html in our templates.
     */
    @GetMapping("/login")
    public String showLoginForm() {
        PaginationHelper.resetSort();
        return "login";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }
}