package com.telerik.addonis.repositories.contracts;

import java.io.IOException;

/**
 * <h1>GitHubApiService</h1>
 * GitHubApiService is used to give the connection to Git Hub Api and provides the necessary data.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
public interface GitHubApiRepository {

    /**
     * This method is used to retrieve the open issue count from github.
     *
     * @param url This is the url from github for which we want to see the data.
     * @return This is the number of Open Issues.
     */
    int getOpenIssueCount(String url) throws IOException;

    /**
     * This method is used to retrieve the pull request count from github.
     *
     * @param url This is the url from github for which we want to see the data.
     * @return This is the number of Pull Requests
     */
    int getPullRequestsCount(String url) throws IOException;

    String getLastCommitDate(String origin) throws IOException;

    String getLastCommitTitle(String origin) throws IOException;


}
