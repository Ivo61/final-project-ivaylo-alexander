package com.telerik.addonis.helpers;

import com.telerik.addonis.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserMapper(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public User updateUser(User originalUser, User user) {
        if (!user.getPassword().isEmpty()) {
            originalUser.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        if (!user.getEmail().isEmpty()) {
            originalUser.setEmail(user.getEmail());
        }
        return originalUser;
    }
}
