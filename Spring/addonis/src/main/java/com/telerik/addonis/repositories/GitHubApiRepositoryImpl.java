package com.telerik.addonis.repositories;

import com.telerik.addonis.repositories.contracts.GitHubApiRepository;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

/**
 * <h1>GitHubApiServiceImpl</h1>
 * GitHubApiServiceImpl is used to get specific data from Git Hub Api.
 * <p>
 * <b>Note:</b> GitHubApiServiceImpl implements the GitHubApiService interface.
 * <b>Note:</b> This is a @Component.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Component
public class GitHubApiRepositoryImpl implements GitHubApiRepository {
    /**
     * This method takes a url and reconfigures it to a git hub api url by utilizing method: dataExtractor,
     * then it receives plain text from a get request to github api and splits the data,which is
     * required and provides us with the open issue count as a number.
     *
     * @param origin This is the url from github.
     * @return This returns a number of Open Issues.
     */
    @Override
    public int getOpenIssueCount(String origin) throws IOException {
        String url = "https://api.github.com/search/issues?q=repo:{0}/{1}%20is:issue%20is:open&per_page=1";
        url = specialLinkFormat(origin, url);
        String line = dataExtractor(url);
        return Arrays.stream(line.split("\"total_count\":")).skip(1).mapToInt(l -> Integer.parseInt(l.split(",")[0])).sum();
    }

    /**
     * This method takes a url and reconfigures it to a git hub api url by utilizing method: dataExtractor,
     * then it receives plain text from a get request to github api and splits the data,which is
     * required and provides us with the pull requests as a number.
     *
     * @param origin This is the url from github.
     * @return This returns a number of Pull Requests.
     */
    @Override
    public int getPullRequestsCount(String origin) throws IOException {
        String url = "https://api.github.com/search/issues?q=repo:{0}/{1}%20is:pr%20is:open&per_page=1";
        url = specialLinkFormat(origin, url);
        String line = dataExtractor(url);
        return Arrays.stream(line.split("\"total_count\":")).skip(1).mapToInt(l -> Integer.parseInt(l.split(",")[0])).sum();
    }

    /**
     * This method takes a url and reconfigures it to a git hub api url by utilizing method: dataExtractor.
     * Plain text is received from a GET request to the github api and the data is split. The split data
     * is then formatted to represent information about the last commit of the product at that url.
     *
     * @param origin This is the url from github.
     * @return This returns a CommitInfo object containing the title and the date of the last commit in different formats.
     */
    @Override
    public String getLastCommitDate(String origin) throws IOException {
        String url = getCommitUrl(origin);
        String line = dataExtractor(url);
        int dateIndex = line.indexOf("date\":");
        List<String> dates = new ArrayList<>();
        dates.add(line.substring(dateIndex + 7, dateIndex + 17));
        return dates.isEmpty() ? "No commits" : dates.get(0);
    }

    public String getLastCommitTitle(String origin) throws IOException {
        String url = getCommitUrl(origin);
        String line = dataExtractor(url);
        line = line.substring(line.indexOf("message\":") + 10);
        return line.substring(0, Math.min(line.indexOf("\\n"), line.indexOf("\"")));
    }

    /**
     * This method reconfigures a normal github url to a github api url, by putting /repos after .com and
     * by putting api. before github.
     * It also provides a header with credentials in order to have at least 5000 request an hour to github api.
     *
     * @param url This is the url to github.
     * @return This returns a string containing the raw text from a get request to github api
     */
    private String dataExtractor(String url) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
        httpURLConnection.addRequestProperty("User-Agent", "Mozilla/5.0");

        String userCredentials = "FinalProjectGitUser:usergitprojectfinal";
        String basicAuth = "Basic " + Base64.getEncoder().encodeToString(userCredentials.getBytes());
        httpURLConnection.setRequestProperty("Authorization", basicAuth);

        BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
        StringBuilder line = new StringBuilder();
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            line.append("/n").append(inputLine);
        }
        in.close();
        return line.toString();
    }

    private String specialLinkFormat(String origin, String url) {
        String[] information = origin.split("/");
        String creator = information[information.length - 2];
        url = url.replace("{0}", creator);
        String addonName = information[information.length - 1];
        url = url.replace("{1}", addonName);
        return url;
    }

    private String getCommitUrl(String origin) {
        String url = origin;
        url = url.replaceFirst("github", "api.github");
        url = url.replaceFirst(".com", ".com/repos");
        url += "/commits";
        return url;
    }
}
