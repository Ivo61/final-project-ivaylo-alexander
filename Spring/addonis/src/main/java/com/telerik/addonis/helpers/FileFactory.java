package com.telerik.addonis.helpers;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <h1>FileFactory</h1>
 * FileFactory is used to convert a multipart file into byte[] in order for
 * it to be saved into the database as a blob.
 * <p>
 * <b>Note:</b> FileFactory is a component.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Component
public class FileFactory {
    /**
     * This method does the conversion from Multipart file into byte[]
     *
     * @param file This is a multipart file.
     * @return This method returns a array of bytes.
     * @throws IOException when the transfer from Multipart file to byte array was unsuccessful.
     */
    public byte[] convertFileToByte(MultipartFile file) throws IOException {
        byte[] byteObjects = new byte[file.getBytes().length];
        file.getContentType();
        int i = 0;

        for (byte b : file.getBytes()) {
            byteObjects[i++] = b;
        }
        return byteObjects;
    }
}
