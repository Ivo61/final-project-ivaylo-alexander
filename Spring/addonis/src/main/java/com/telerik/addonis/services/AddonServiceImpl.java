package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.helpers.AddonMapper;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.Status;
import com.telerik.addonis.models.Tag;
import com.telerik.addonis.repositories.AddonRepository;
import com.telerik.addonis.services.contracts.AddonService;
import com.telerik.addonis.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.telerik.addonis.constants.AddonStatus.*;

/**
 * <h1>AddonServiceImpl</h1>
 * AddonServiceImpl is used to give access to the database when dealing with objects of the Addon Class,
 * it also provides the logic.
 * <p>
 * <b>Note:</b> AddonServiceImpl implements the AddonService interface.
 * <b>Note:</b> This is a @Service.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Service
public class AddonServiceImpl implements AddonService {
    /**
     * These are all the @Components and @Services we need in order to implement our logic.
     */
    private final AddonRepository repository;
    private final StatusService statusService;
    private final AddonMapper mapper;

    /**
     * This is the constructor for the AddonServiceImpl objects, generating all the @Components and @Services needed.
     */
    @Autowired
    public AddonServiceImpl(AddonRepository repository,
                            StatusService statusService,
                            AddonMapper mapper) {
        this.repository = repository;
        this.statusService = statusService;
        this.mapper = mapper;
    }

    /**
     * This method is used to retrieve all Addons in our database.
     *
     * @return This returns all of the addons in our database as a list.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Addon> getAll() {
        return repository.findAll();
    }

    /**
     * This method is used to retrieve an Addon object from the repository
     * by the given name field of the object.
     *
     * @param addonName This is the name of the Addon object.
     * @return This method returns the Addon object defined by the name parameter.
     * @throws EntityNotFoundException when no Addon with such a name could be found
     */
    @Override
    @Transactional(readOnly = true)
    public Addon getByName(String addonName) {
        throwIfAddonDoesNotExist(addonName);
        return repository.findByName(addonName);
    }

    /**
     * This method is used to retrieve an Addon object from the repository
     * by the given id field of the object.
     *
     * @param id This is the id of the Addon object.
     * @return This method returns the Addon object defined by the name parameter.
     * @throws EntityNotFoundException when no Addon with such an id could be found
     */
    @Override
    @Transactional(readOnly = true)
    public Addon getById(Integer id) {
        throwIfAddonDoesNotExist(id);
        return repository.findById(id).orElse(null);
    }

    /**
     * This method is used to create an Addon in our database.
     *
     * @param addon This is an Addon object, containing the data for the Addon object's fields.
     * @return This returns the created Addon object.
     */
    @Override
    public Addon create(Addon addon) {
        throwIfAddonAlreadyExists(addon);
        return repository.save(addon);
    }

    private void throwIfAddonAlreadyExists(Addon addon) {
        if (repository.existsByName(addon.getName())) {
            throw new DuplicateEntityException("Addon", addon.getName());
        }
    }

    /**
     * This method is used to update an existing Addon in our database.
     *
     * @param addon This is an Addon object, that is to be edited.
     * @return This returns the edited Addon object.
     */
    @Override
    public Addon update(int id, Addon addon) {
        throwIfAddonDoesNotExist(id);
        throwIfAddonAlreadyExists(addon);
        return repository.save(addon);
    }

    /**
     * This method is used to update an existing Addon in our database.
     *
     * @param addonToBeUpdated This is an Addon object, that is to be edited.
     * @return This returns the edited Addon object.
     */
    @Override
    public Addon update(Addon addonToBeUpdated) {
        throwIfAddonDoesNotExist(addonToBeUpdated.getName());
        return repository.save(addonToBeUpdated);
    }

    /**
     * This method is used to soft delete an existing Addon in our database. The Addon object's Set of Status objects is
     * cleared and single Deleted status is added to it to represent the Addon being unavailable.
     *
     * @param addonName This is name of the Addon object that is to be soft deleted.
     * @return This returns the soft deleted Addon object.
     */
    @Override
    public Addon delete(String addonName) {
        throwIfAddonDoesNotExist(addonName);
        Set<Status> status = new HashSet<>();
        status.add(statusService.getByName(DELETED));
        Addon deletedAddon = getByName(addonName);
        deletedAddon.getStatuses().clear();
        deletedAddon.setStatuses(status);
        return repository.save(deletedAddon);
    }

    /**
     * This method is used to retrieve all Addons in our database created by a certain User and having a certain status.
     *
     * @param status   This is the name of the Status being searched for in all Addons.
     * @param username This is the name of the User being searched for in all Addons.
     * @return This returns all of the addons in our database that were created by the user with the given name and
     * contain the status given, as a list.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Addon> getByUserAndStatus(String status, String username) {
        List<Addon> result = filterByUser(getAll(), username);
        return filterByStatus(result, status);
    }

    /**
     * This method is used to filter Addons in a by their creator User.
     *
     * @param addons   This is a list of Addon objects that is being filtered.
     * @param username This is the name of the User being searched for in the list.
     * @return This returns a list of all of the addons in the list given that were created by the user with the given name.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Addon> filterByUser(List<Addon> addons, String username) {
        return addons
                .stream()
                .filter(addon -> (username == null || addon.getCreator().contains(username)))
                .collect(Collectors.toList());
    }

    /**
     * This method is used to filter Addons in a list by their statuses.
     *
     * @param addons      This is a list of Addon objects that is being filtered.
     * @param addonStatus This is the name of the Status being searched for in the list.
     * @return This returns a list of all of the addons in the list that contained the status given.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Addon> filterByStatus(List<Addon> addons, String addonStatus) {
        Status status = statusService.getByName(addonStatus);
        return addons
                .stream()
                .filter(addon -> addon.getStatuses().contains(status))
                .collect(Collectors.toList());
    }

    /**
     * This method is used to retrieve a list of all addons with an upload date that is between the current moment
     * and a given point back in time.
     *
     * @param addons       This is a list of addons from which to check for.
     * @param timeInterval This is the Long in milliseconds defining how far back in time to check for.
     * @return This returns a list of Addon objects, sorted in descending order of their uploadDate field.
     */
    @Override
    public List<Addon> filterByCreationDate(List<Addon> addons, Long timeInterval) {
        Date falloffPoint = new Date(System.currentTimeMillis() - timeInterval);
        return addons
                .stream()
                .filter(addon -> addon.getUploadDate().after(falloffPoint))
                .sorted((a1, a2) -> a2.getUploadDate().compareTo(a1.getUploadDate()))
                .collect(Collectors.toList());
    }

    /**
     * This method is used to retrieve a list of all addons, sorted by their number of downloads
     *
     * @param addons This is a list of addons from which to check for.
     * @return This returns a list of Addon objects, sorted in descending order of their numberOfDownloads field.
     */
    @Override
    public List<Addon> sortByDownloadCount(List<Addon> addons) {
        return addons
                .stream()
                .sorted((a1, a2) -> a2.getNumberOfDownloads().compareTo(a1.getNumberOfDownloads()))
                .collect(Collectors.toList());
    }

    /**
     * This method is used to change the Set of Status objects of an Addon so that it contains the Featured Status if
     * it does not already, or to remove it if it already contains it.
     *
     * @param addonName This is the name of the Addon being edited.
     */
    @Override
    public Addon toggleFeatured(String addonName) {
        throwIfAddonDoesNotExist(addonName);
        Addon addon = getByName(addonName);
        Set<Status> statuses = addon.getStatuses();
        Status featured = statusService.getByName(FEATURED);
        if (!statuses.contains(featured)) {
            statuses.add(featured);
        } else {
            statuses.remove(featured);
        }
        addon.setStatuses(statuses);
        return repository.save(addon);
    }

    /**
     * This method is used to change the Set of Status objects of an Addon so that it no longer contains a Pending status
     * and contains an Approved Status.
     *
     * @param addonName This is the name of the Addon being edited.
     */
    @Override
    public Addon approve(String addonName) {
        return resolvePending(addonName, APPROVED);
    }

    /**
     * This method is used to change the Set of Status objects of an Addon so that it no longer contains a Pending status
     * and contains an Approved Status.
     *
     * @param addonName This is the name of the Addon being edited.
     */
    @Override
    public Addon reject(String addonName) {
        return resolvePending(addonName, REJECTED);
    }

    /**
     * This method edits the Set of Status objects of a Addon so that it no longer contains a Pending status
     * and adds a new Status depending on the parameters given.
     *
     * @param addonName  This is the name of the Addon being edited.
     * @param statusName This is the name of the Status that is replacing the Pending Status.
     * @return This returns the edited Addon object.
     */
    private Addon resolvePending(String addonName, String statusName) {
        throwIfAddonDoesNotExist(addonName);
        Addon editedAddon = getByName(addonName);
        Set<Status> statuses = editedAddon.getStatuses();
        Status newStatus = statusService.getByName(statusName);
        Status pending = statusService.getByName(PENDING);
        statuses.remove(pending);
        statuses.add(newStatus);
        editedAddon.setStatuses(statuses);
        return repository.save(editedAddon);
    }

    /**
     * This method gives the names of all the statuses that an Addon object has.
     *
     * @param addonName This is the name of the Addon whose Status names are being retrieved.
     * @return This returns a String containing the name fields of the Status objects associated with
     * the given Addon.
     */
    @Override
    public String getStatusNames(String addonName) {
        throwIfAddonDoesNotExist(addonName);
        return getByName(addonName)
                .getStatuses()
                .stream()
                .map(Status::getName)
                .collect(Collectors.toList()).toString();
    }

    /**
     * This method gives the names of all the tags that an Addon object has.
     *
     * @param addonName This is the name of the Addon whose Tag names are being retrieved.
     * @return This returns a String containing the name fields of the Tag objects associated with
     * the given Addon, separated by a comma and preceded by a hash tag for proper validation in the
     * html template.
     */
    @Override
    public String getTagNames(String addonName) {
        throwIfAddonDoesNotExist(addonName);
        StringBuilder tagNames = new StringBuilder();
        Set<Tag> tags = getByName(addonName).getTags();
        for (Tag tag : tags) {
            tagNames.append("#");
            tagNames.append(tag.getName());
            tagNames.append(",");
        }
        if (!(tagNames.length() == 0)) {
            tagNames.deleteCharAt(tagNames.length() - 1);
        }
        return tagNames.toString();
    }

    /**
     * This method throws an exception if an Addon name does not exist in the repository.
     *
     * @param addonName This is the name of the Addon being searched for.
     * @throws EntityNotFoundException when such an Addon is not present cannot be found by it's name field.
     */
    private void throwIfAddonDoesNotExist(String addonName) {
        if (!repository.existsByName(addonName)) {
            throw new EntityNotFoundException("Addon", addonName);
        }
    }

    /**
     * This method throws an exception if an Addon id does not exist in the repository.
     *
     * @param id This is the id of the Addon being searched for.
     * @throws EntityNotFoundException when such an Addon is not present cannot be found by it's id field.
     */
    private void throwIfAddonDoesNotExist(Integer id) {
        if (!repository.existsById(id)) {
            throw new EntityNotFoundException("Addon", "ID:" + id);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Addon> paginationWithSortByStatus(String sort, int order, Pageable pageable) {
        switch (sort) {
            case "Number Of Downloads":
                if (order == 0) {
                    return repository.findAllByStatuses_NameOrderByNumberOfDownloadsDesc(APPROVED, pageable);
                } else {
                    return repository.findAllByStatuses_NameOrderByNumberOfDownloadsAsc(APPROVED, pageable);
                }
            case "Upload Date":
                if (order == 0) {
                    return repository.findAllByStatuses_NameOrderByUploadDateDesc(APPROVED, pageable);
                } else {
                    return repository.findAllByStatuses_NameOrderByUploadDateAsc(APPROVED, pageable);
                }
            default:
                if (order == 0) {
                    return repository.findAllByStatuses_NameOrderByNameAsc(APPROVED, pageable);
                } else {
                    return repository.findAllByStatuses_NameOrderByNameDesc(APPROVED, pageable);
                }
        }
    }


    public Addon updateNumberOfDownloads(String addonName) {
        Addon addon = getByName(addonName);
        addon.setNumberOfDownloads(addon.getNumberOfDownloads() + 1);
        return repository.save(addon);
    }

    @Override
    public List<Addon> filter(String keyword, String whereToLook) {
        if ("Tags".equals(whereToLook)) {
            return getAll().stream().filter(addon -> addon.getTags().stream()
                    .anyMatch(tag -> tag.getName().equalsIgnoreCase(keyword.toLowerCase())))
                    .collect(Collectors.toList());
        } else {
            return repository.findByNameContainingIgnoreCase(keyword);
        }
    }

    @Override
    public void removeTagFromAddons(Tag tag) {
        String tagName = tag.getName();
        List<Addon> containingThisTag = filter(tagName, "Tags");
        for (Addon addon : containingThisTag) {
            addon.getTags().remove(tag);
            update(addon);
        }
    }
}
