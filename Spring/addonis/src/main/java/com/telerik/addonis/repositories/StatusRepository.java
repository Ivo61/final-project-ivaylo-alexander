package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <h1>StatusRepository</h1>
 * StatusRepository is used to access our database.
 * <p>
 * <b>Note:</b> StatusRepository extends the JpaRepository interface.
 * <p>
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
@Repository
public interface StatusRepository extends JpaRepository<Status, Integer> {
    /**
     * This method is used to retrieve a Status object from the repository
     * by the given name field of the object.
     *
     * @param name This is the name of the Status object
     * @return This method returns the Status object.
     */
    Status findByName(String name);

    /**
     * This method is used to check if a specific status exists.
     *
     * @param name This is the name of the Status object
     * @return This boolean variable tells us if the Status exists.
     */
    boolean existsByName(String name);
}
