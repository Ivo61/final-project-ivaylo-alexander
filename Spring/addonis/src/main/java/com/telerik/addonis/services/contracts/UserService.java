package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.User;

import java.util.List;

/**
 * <h1>UserService</h1>
 * UserService is used to give the logic of our application regarding users.
 *
 * @author Alexander Stoychev, Ivaylo Staykov
 * @version 1.0
 * @since 2020-04-15
 */
public interface UserService {
    /**
     * This method is used to retrieve all Users in our database.
     *
     * @return This returns all of the users in our database as a list.
     */
    List<User> getAll();

    /**
     * This method is used to retrieve a specific User by its name.
     *
     * @param userName This is the name of the User object
     * @return This returns a User object.
     */
    User getByName(String userName);

    /**
     * This method is used to create a new User.
     *
     * @param user This is a User object
     * @return This returns a User object.
     */
    User register(User user);

    /**
     * This method is used to update an existing User.
     *
     * @param user This is an User object
     * @return This returns a User object.
     */
    User update(User user);

    /**
     * This method is used to disable an existing User.
     *
     * @param username This is the username of the User object.
     * @return This returns a User object.
     */
    User disableUser(String username);

    User enableUser(String username);

}
