function tagHash() {
    let currentTag = document.getElementById("visualizeTags").value.split(',');
    let allTags = document.getElementById("allTags").innerText.split(", ");
    allTags[0] = allTags[0].substring(1);
    allTags[allTags.length - 1] = allTags[allTags.length - 1].substring(0, allTags[allTags.length - 1].length - 1);
    let result = currentTag.map(function (word) {
        if (word[0] !== '#' && word[0] !== '@') {
            if (allTags.includes(word)) {
                if (word[0] !== '#') {
                    word = '#' + word;
                    return word;
                }
            }
            return "@" + word;
        }
        return word;
    })
    let placeholder = currentTag.map(function (word) {
        if (word[0] !== '@') {
            if (allTags.includes(word)) {
                if (word[0] !== '#') {
                    return word;
                }
            }
            if (allTags.includes(word.substring(1))) {
                return word.substring(1);
            }
        }
    })
    let tag = document.getElementById("visualizeTags");
    let actualTags = document.getElementById("tags")
    tag.value = result;
    actualTags.value = placeholder;
}