package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.helpers.UserMapper;
import com.telerik.addonis.models.User;
import com.telerik.addonis.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.ArrayList;
import java.util.List;

import static com.telerik.addonis.Factory.createUser;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {
    @Mock
    UserRepository userRepository;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    UserDetailsManager userDetailsManager;
    @Mock
    UserMapper mapper;

    @InjectMocks
    UserServiceImpl mockService;

    @Test
    public void getAll_should_return_when_usersExists() {
        //Arrange
        User sampleUser =createUser();
        List<User> expectedList = new ArrayList<>();
        expectedList.add(sampleUser);
        Mockito.when(userRepository.findAll()).thenReturn(expectedList);

        //Act
        List<User> returnedList = mockService.getAll();

        //Assert
        Assert.assertNotNull(returnedList);
    }

    @Test
    public void create_should_createUser_when_inputIsValid() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(userRepository.save(any())).thenReturn(expectedUser);
        Mockito.when(userRepository.existsByUsername(anyString())).thenReturn(false);
        Mockito.when(passwordEncoder.encode(any())).thenReturn("g");

        //Act
        User returnedUser = mockService.register(createUser());

        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createUser_should_throw_when_userAlreadyExist() {
        //Arrange
        User anyUser = createUser();
        Mockito.when(userRepository.save(any())).thenThrow(new DuplicateEntityException("a", "b"));
        Mockito.when(passwordEncoder.encode(any())).thenReturn("g");

        //Act, Assert
        mockService.register(anyUser);
    }

    @Test
    public void update_should_returnUser_when_userExists() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(userRepository.save(any())).thenReturn(expectedUser);
        Mockito.when(userRepository.existsByUsername(anyString())).thenReturn(true);

        //Act
        User returnedUser = mockService.update(createUser());

        //Assert
        Assert.assertEquals(returnedUser, expectedUser);
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_should_throw_when_userDoesNotExist() {
        //Arrange
        Mockito.when(userRepository.existsByUsername(any())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.update(createUser());
    }

    @Test
    public void disable_should_changeEnabled_when_inputIsValid() {
        //Arrange
        User expectedUser = createUser();
        expectedUser.setEnabled((byte) 1);
        Mockito.when(userRepository.existsByUsername(anyString())).thenReturn(true);
        Mockito.when(userRepository.findByUsername(any())).thenReturn(expectedUser);
        Mockito.when(userRepository.save(any())).thenReturn(expectedUser);

        //Act
        User returnedUser = mockService.disableUser(anyString());

        //Assert
        Assert.assertEquals(returnedUser.getEnabled(), 0);
    }

    @Test(expected = EntityNotFoundException.class)
    public void disable_should_throw_when_userDoesNotExist() {
        //Arrange
        Mockito.when(userRepository.existsByUsername(any())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.disableUser(anyString());
    }

    @Test
    public void enable_should_changeEnabled_when_inputIsValid() {
        //Arrange
        User expectedUser = createUser();
        expectedUser.setEnabled((byte) 0);
        Mockito.when(userRepository.existsByUsername(anyString())).thenReturn(true);
        Mockito.when(userRepository.findByUsername(any())).thenReturn(expectedUser);
        Mockito.when(userRepository.save(any())).thenReturn(expectedUser);

        //Act
        User returnedUser = mockService.enableUser(anyString());

        //Assert
        Assert.assertEquals(returnedUser.getEnabled(), 1);
    }

    @Test(expected = EntityNotFoundException.class)
    public void enable_should_throw_when_userDoesNotExist() {
        //Arrange
        Mockito.when(userRepository.existsByUsername(any())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.enableUser(anyString());
    }
}
