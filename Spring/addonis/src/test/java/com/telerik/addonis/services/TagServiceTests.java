package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Tag;
import com.telerik.addonis.repositories.TagRepository;
import org.junit.Assert;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.telerik.addonis.Factory.createTag;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTests {
    @Mock
    TagRepository tagRepository;

    @InjectMocks
    TagServiceImpl mockService;

    @Test
    public void getAll_should_return_when_tagsExist() {
        //Arrange
        List<Tag> expectedList = new ArrayList<>();
        Mockito.when(tagRepository.findAll()).thenReturn(expectedList);

        //Act
        List<Tag> returnedList = mockService.getAll();

        //Assert
        Assert.assertNotNull(returnedList);
    }

    @Test
    public void getByName_should_returnTag_when_tagExists() {
        //Arrange
        Tag expectedTag = createTag();
        Mockito.when(tagRepository.findByName(anyString())).thenReturn(expectedTag);
        Mockito.when(tagRepository.existsByName(anyString())).thenReturn(true);

        //Act
        Tag returnedTag = mockService.getByName("q");

        //Assert
        Assert.assertSame(expectedTag, returnedTag);
        Assert.assertEquals(expectedTag.getId(), returnedTag.getId());
        Assert.assertEquals(expectedTag.getName(), returnedTag.getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByName_should_throw_when_tagDoesNotExist() {
        //Arrange
        Mockito.when(tagRepository.existsByName(anyString())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.getByName("p");
    }

    @Test
    public void delete_should_deleteTag_when_inputIsValid() {
        //Arrange
        Tag expectedTag = createTag();
        Mockito.when(tagRepository.existsByName(any())).thenReturn(true);
        Mockito.when(tagRepository.findByName(any())).thenReturn(expectedTag);

        //Act
        Tag returnedTag = mockService.delete(any());

        //Assert
        Assert.assertSame(expectedTag, returnedTag);
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteTag_should_throw_when_tagDoesNotExist() {
        //Arrange
        Mockito.when(tagRepository.existsByName(anyString())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.delete("g");
    }

    @Test
    public void create_should_createTag_when_inputIsValid() {
        //Arrange
        Tag expectedTag = createTag();
        Mockito.when(tagRepository.save(any())).thenReturn(expectedTag);
        Mockito.when(tagRepository.existsByName(anyString())).thenReturn(false);

        //Act
        Tag returnedTag = mockService.create(createTag());

        //Assert
        Assert.assertSame(expectedTag, returnedTag);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createTag_should_throw_when_tagAlreadyExist() {
        //Arrange
        Tag anyTag = createTag();
        Mockito.when(tagRepository.save(any())).thenThrow(new DuplicateEntityException("a", "b"));

        //Act, Assert
        mockService.create(anyTag);
    }

    @Test
    public void update_should_returnTag_when_tagExists() {
        //Arrange
        Tag expectedTag = createTag();
        Mockito.when(tagRepository.save(any())).thenReturn(expectedTag);
        Mockito.when(tagRepository.existsByName(anyString())).thenReturn(true);

        //Act
        Tag returnedTag = mockService.update(createTag());

        //Assert
        Assert.assertEquals(returnedTag, expectedTag);
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_should_throw_when_tagDoesNotExist() {
        //Arrange
        Mockito.when(tagRepository.existsByName(any())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.update(createTag());
    }

}
