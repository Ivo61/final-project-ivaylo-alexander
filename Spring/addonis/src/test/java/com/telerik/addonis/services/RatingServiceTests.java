package com.telerik.addonis.services;

import com.telerik.addonis.helpers.SecurityUserHelper;
import com.telerik.addonis.models.Rating;
import com.telerik.addonis.repositories.RatingRepository;
import com.telerik.addonis.services.contracts.AddonService;
import com.telerik.addonis.services.contracts.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.telerik.addonis.Factory.createRating;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class RatingServiceTests {
    @Mock
    RatingRepository ratingRepository;
    @Mock
    UserService userService;
    @Mock
    AddonService addonService;
    @Mock
    SecurityUserHelper currentUser;

    @InjectMocks
    RatingServiceImpl mockService;

    @Test
    public void getAll_should_return_when_ratingsExist() {
        //Arrange
        Rating sampleRating = createRating();
        List<Rating> expectedList = new ArrayList<>();
        expectedList.add(sampleRating);
        Mockito.when(ratingRepository.findAll()).thenReturn(expectedList);

        //Act
        List<Rating> returnedList = mockService.getAll();

        //Assert
        Assert.assertNotNull(returnedList);
    }

    @Test
    public void rate_should_createRating_when_inputIsValid(){
        //Arrange
        Rating expectedRating = createRating();
        Mockito.when(ratingRepository.save(any())).thenReturn(expectedRating);

        //Act
        Rating returnedRating = mockService.rate("a","4");

        //Assert
        Assert.assertSame(expectedRating, returnedRating);
    }

    @Test
    public void getAverageRatingForAddon_should_return_when_ratingExists(){
        //Arrange
        Rating expectedRating = createRating();
        List<Rating> ratingsList = new ArrayList<>();
        ratingsList.add(expectedRating);
        Double expectedAverage = (double) expectedRating.getRating();
        Mockito.when(ratingRepository.getByAddonName(any())).thenReturn(ratingsList);

        //Act
        Double returnedAverage = mockService.getAverageRatingForAddon("a");

        //Assert
        Assert.assertEquals(expectedAverage, returnedAverage);
    }
}
