package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Content;
import com.telerik.addonis.repositories.ContentRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.telerik.addonis.Factory.createContent;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class ContentServiceTests {
    @Mock
    ContentRepository contentRepository;

    @InjectMocks
    ContentServiceImpl mockService;

    @Test
    public void getByName_should_returnAddonContent_when_addonExists() {
        //Arrange
        Content expectedContent = createContent();
        Mockito.when(contentRepository.existsByName(anyString())).thenReturn(true);
        Mockito.when(contentRepository.findByName(anyString())).thenReturn(expectedContent);

        //Act
        Content returnedContent = mockService.getByName("q");

        //Assert
        Assert.assertSame(expectedContent, returnedContent);
        Assert.assertEquals(expectedContent.getId(), returnedContent.getId());
        Assert.assertEquals(expectedContent.getName(), returnedContent.getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByName_should_throw_when_addonDoesNotExist() {
        //Arrange
        Mockito.when(contentRepository.existsByName(anyString())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.getByName("p");
    }

    @Test
    public void create_should_createContent_when_inputIsValid(){
        //Arrange
        Content expectedContent = createContent();
        Mockito.when(contentRepository.save(any())).thenReturn(expectedContent);

        //Act
        Content returnedContent = mockService.create(createContent());

        //Assert
        Assert.assertSame(expectedContent, returnedContent);
    }
}
