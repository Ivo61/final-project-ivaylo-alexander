package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Status;
import com.telerik.addonis.repositories.StatusRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.telerik.addonis.Factory.createStatus;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class StatusServiceTests {
    @Mock
    StatusRepository statusRepository;

    @InjectMocks
    StatusServiceImpl mockService;

    @Test
    public void getByName_should_returnStatus_when_addonExists() {
        //Arrange
        Status expectedStatus = createStatus();
        Mockito.when(statusRepository.findByName(anyString())).thenReturn(expectedStatus);
        Mockito.when(statusRepository.existsByName(anyString())).thenReturn(true);

        //Act
        Status returnedStatus = mockService.getByName("q");

        //Assert
        Assert.assertSame(expectedStatus, returnedStatus);
        Assert.assertEquals(expectedStatus.getId(), returnedStatus.getId());
        Assert.assertEquals(expectedStatus.getName(), returnedStatus.getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByName_should_throw_when_addonDoesNotExist() {
        //Arrange
        Mockito.when(statusRepository.existsByName(anyString())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.getByName("p");
    }

}
