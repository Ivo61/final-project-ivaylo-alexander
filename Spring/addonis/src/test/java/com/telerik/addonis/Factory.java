package com.telerik.addonis;

import com.telerik.addonis.models.*;
import com.telerik.addonis.models.dto.AddonDTO;
import org.springframework.data.domain.PageRequest;

import org.springframework.data.domain.Pageable;

import static com.telerik.addonis.constants.PageSizes.DEFAULT_PAGE_LIMIT;

public class Factory {

    public static Tag createTag() {
        Tag newTag = new Tag();
        newTag.setName("Test Tag");
        return newTag;
    }

    public static User createUser() {
        User newUser = new User();
        newUser.setUsername("Test User");
        newUser.setPassword("password");
        newUser.setPasswordConfirmation("password");
        return newUser;
    }

    public static Addon createAddon(){
        Addon newAddon = new Addon();
        newAddon.setName("New Addon");
        return newAddon;
    }

    public static AddonDTO createAddonDTO(){
        AddonDTO newDTO = new AddonDTO();
        newDTO.setName("New DTO");
        newDTO.setDescription("Description");
        return newDTO;
    }

    public static Status createStatus(){
        Status newStatus = new Status();
        newStatus.setName("Featured");
        return newStatus;
    }

    public static Status createStatus(String statusName){
        Status newStatus = new Status();
        newStatus.setName(statusName);
        return newStatus;
    }

    public static Rating createRating(){
        Rating newRating = new Rating();
        newRating.setRating(4);
        return newRating;
    }

    public static Content createContent(){
        Content newContent = new Content();
        newContent.setName("Content");
        return newContent;
    }

    public static Status createDeletedStatus(){
        Status newStatus = new Status();
        newStatus.setName("Deleted");
        return newStatus;
    }

    public static Pageable createPageable(){
        Pageable newPageable = PageRequest.of(1, 10);
        return newPageable;
    }
}
