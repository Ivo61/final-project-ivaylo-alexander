package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.helpers.AddonMapper;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.Status;
import com.telerik.addonis.models.Tag;
import com.telerik.addonis.repositories.AddonRepository;
import com.telerik.addonis.services.contracts.StatusService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.*;

import static com.telerik.addonis.Factory.*;
import static com.telerik.addonis.constants.TimeIntervals.DAY;
import static com.telerik.addonis.constants.TimeIntervals.WEEK;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class AddonServiceTests {
    @Mock
    AddonRepository addonRepository;
    @Mock
    AddonMapper mapper;
    @Mock
    StatusService statusService;

    @InjectMocks
    AddonServiceImpl mockService;

    @Test
    public void getAll_should_return_when_addonsExist() {
        //Arrange
        Addon sampleAddon = createAddon();
        List<Addon> expectedList = new ArrayList<>();
        expectedList.add(sampleAddon);
        Mockito.when(addonRepository.findAll()).thenReturn(expectedList);

        //Act
        List<Addon> returnedList = mockService.getAll();

        //Assert
        Assert.assertNotNull(returnedList);
    }

    @Test
    public void getByName_should_returnAddon_when_addonExists() {
        //Arrange
        Addon expectedAddon = createAddon();
        Mockito.when(addonRepository.findByName(anyString())).thenReturn(expectedAddon);
        Mockito.when(addonRepository.existsByName(anyString())).thenReturn(true);

        //Act
        Addon returnedAddon = mockService.getByName("q");

        //Assert
        Assert.assertSame(expectedAddon, returnedAddon);
        Assert.assertEquals(expectedAddon.getId(), returnedAddon.getId());
        Assert.assertEquals(expectedAddon.getName(), returnedAddon.getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByName_should_throw_when_addonDoesNotExist() {
        //Arrange
        Mockito.when(addonRepository.existsByName(anyString())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.getByName("p");
    }

    @Test
    public void getById_should_returnAddon_when_addonExists() {
        //Arrange
        Optional<Addon> expectedAddon = Optional.of(createAddon());
        Mockito.when(addonRepository.findById(anyInt())).thenReturn(expectedAddon);
        Mockito.when(addonRepository.existsById(anyInt())).thenReturn(true);

        //Act
        Addon returnedAddon = mockService.getById(7);

        //Assert
        Assert.assertSame(expectedAddon.get(), returnedAddon);
        Assert.assertEquals(expectedAddon.get().getId(), returnedAddon.getId());
        Assert.assertEquals(expectedAddon.get().getName(), returnedAddon.getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getById_should_throw_when_addonDoesNotExist() {
        //Arrange
        Mockito.when(addonRepository.existsById(anyInt())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.getById(7);
    }

    @Test
    public void create_should_createAddon_when_inputIsValid(){
        //Arrange
        Addon expectedAddon = createAddon();
        Mockito.when(addonRepository.save(any())).thenReturn(expectedAddon);
        Mockito.when(addonRepository.existsByName(anyString())).thenReturn(false);

        //Act

        Addon returnedAddon = mockService.create(createAddon());

        //Assert
        Assert.assertSame(expectedAddon, returnedAddon);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createAddon_should_throw_when_addonAlreadyExist(){
        //Arrange
        Mockito.when(addonRepository.save(any())).thenThrow(new DuplicateEntityException("a", "b"));

        //Act, Assert
        mockService.create(createAddon());
    }

    @Test
    public void update_should_returnAddon_when_addonExists() {
        //Arrange
        Addon expectedAddon = createAddon();
        Mockito.when(addonRepository.save(any())).thenReturn(expectedAddon);
        Mockito.when(addonRepository.existsByName(anyString())).thenReturn(true);

        //Act
        Addon returnedAddon = mockService.update(createAddon());

        //Assert
        Assert.assertEquals(returnedAddon, expectedAddon);
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_should_throw_when_addonDoesNotExist() {
        //Arrange
        Mockito.when(addonRepository.existsByName(any())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.update(createAddon());
    }

    @Test(expected = DuplicateEntityException.class)
    public void update_should_throw_when_addonAlreadyExist(){
        //Arrange
        Mockito.when(addonRepository.existsByName(anyString())).thenReturn(true);
        Mockito.when(addonRepository.save(any())).thenThrow(new DuplicateEntityException("a", "b"));

        //Act, Assert
        mockService.update(createAddon());
    }

    @Test
    public void updateWithParameters_should_returnAddon_when_addonExists(){
        //Arrange
        Addon expectedAddon = createAddon();
        expectedAddon.setId(7);
        Mockito.when(addonRepository.save(any())).thenReturn(expectedAddon);
        Mockito.when(addonRepository.existsById(anyInt())).thenReturn(true);

        //Act
        Addon returnedAddon = mockService.update(expectedAddon.getId(),createAddon());

        //Assert
        Assert.assertEquals(returnedAddon, expectedAddon);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateWithParameters_should_throw_when_addonDoesNotExist(){
        //Arrange
        Mockito.when(addonRepository.existsById(anyInt())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.update(7,createAddon());
    }

    @Test
    public void delete_should_deleteAddon_when_inputIsValid() {
        //Arrange
        Addon expectedAddon = createAddon();
        Set<Status> expectedStatuses = new HashSet<>();
        Status status = createDeletedStatus();
        expectedStatuses.add(status);
        expectedAddon.setStatuses(expectedStatuses);
        Mockito.when(addonRepository.existsByName(any())).thenReturn(true);
        Mockito.when(addonRepository.findByName(any())).thenReturn(expectedAddon);
        Mockito.when(addonRepository.save(any())).thenReturn(expectedAddon);
        Mockito.when(statusService.getByName(any())).thenReturn(status);

        //Act
        Addon returnedAddon = mockService.delete(any());

        //Assert
        expectedStatuses.add(createDeletedStatus());
        Assert.assertSame(expectedAddon, returnedAddon);
        Assert.assertSame(expectedAddon.getStatuses(), returnedAddon.getStatuses());
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteAddon_should_throw_when_addonDoesNotExist() {
        //Arrange
        Mockito.when(addonRepository.existsByName(anyString())).thenThrow(new EntityNotFoundException("o"));

        //Act, Assert
        mockService.delete("g");
    }

    @Test
    public void toggleFeatured_should_changeStatus_when_addonExists() {
        //Arrange
        Addon expectedAddon = createAddon();
        Status status = createStatus();
        Set<Status> expectedStatuses = new HashSet<>();
        expectedStatuses.add(status);
        expectedAddon.setStatuses(expectedStatuses);
        Mockito.when(addonRepository.existsByName(anyString())).thenReturn(true);
        Mockito.when(statusService.getByName(anyString())).thenReturn(status);
        Mockito.when(addonRepository.findByName(any())).thenReturn(expectedAddon);
        Mockito.when(addonRepository.save(any())).thenReturn(expectedAddon);

        //Act
        Addon returnedAddon = mockService.toggleFeatured("q");

        //Assert
        Assert.assertEquals(returnedAddon, expectedAddon);
        Assert.assertFalse(returnedAddon.getStatuses().contains(status));
    }

    @Test
    public void approve_should_returnAddon_when_addonExists() {
        //Arrange
        Addon expectedAddon = createAddon();
        Status status = createStatus("Approved");
        Set<Status> expectedStatuses = new HashSet<>();
        expectedStatuses.add(status);
        expectedAddon.setStatuses(expectedStatuses);
        Mockito.when(addonRepository.existsByName(anyString())).thenReturn(true);
        Mockito.when(addonRepository.findByName(any())).thenReturn(expectedAddon);
        Mockito.when(addonRepository.save(any())).thenReturn(expectedAddon);
        Mockito.when(statusService.getByName(anyString())).thenReturn(status);

        //Act
        Addon returnedAddon = mockService.approve("q");

        //Assert
        Assert.assertEquals(expectedAddon,returnedAddon);
        Assert.assertTrue(returnedAddon.getStatuses().contains(status));
    }

    @Test
    public void reject_should_returnAddon_when_addonExists() {
        //Arrange
        Addon expectedAddon = createAddon();
        Status status = createStatus("Rejected");
        Set<Status> expectedStatuses = new HashSet<>();
        expectedStatuses.add(status);
        expectedAddon.setStatuses(expectedStatuses);
        Mockito.when(addonRepository.existsByName(anyString())).thenReturn(true);
        Mockito.when(addonRepository.findByName(any())).thenReturn(expectedAddon);
        Mockito.when(addonRepository.save(any())).thenReturn(expectedAddon);
        Mockito.when(statusService.getByName(anyString())).thenReturn(status);

        //Act
        Addon returnedAddon = mockService.reject("q");

        //Assert
        Assert.assertEquals(expectedAddon,returnedAddon);
        Assert.assertTrue(returnedAddon.getStatuses().contains(status));
    }

    @Test
    public void updateNumberOfDownloads_should_returnAddon_when_addonExists(){
        //Arrange
        Addon expectedAddon = createAddon();
        expectedAddon.setNumberOfDownloads(7);
        Mockito.when(addonRepository.existsByName(anyString())).thenReturn(true);
        Mockito.when(addonRepository.findByName(anyString())).thenReturn(expectedAddon);
        Mockito.when(addonRepository.save(any())).thenReturn(expectedAddon);

        //Act
        Addon returnedAddon = mockService.updateNumberOfDownloads("p");

        //Assert
        Assert.assertTrue(returnedAddon.getNumberOfDownloads()!=7);
    }

    @Test
    public void filter_should_returnAddons_when_addonExists(){
        //Arrange
        Addon expectedAddonOne = createAddon();
        Addon expectedAddonTwo = createAddon();
        Addon notExpectedAddon = createAddon();
        expectedAddonOne.setName("OneOne");
        expectedAddonTwo.setName("OneTwo");
        notExpectedAddon.setName("TwoTwo");
        List<Addon> resultList = new ArrayList<>();
        resultList.add(expectedAddonOne);
        resultList.add(expectedAddonTwo);
        resultList.add(notExpectedAddon);
        Mockito.when(addonRepository.findByNameContainingIgnoreCase(anyString())).thenReturn(resultList);

        //Act
        List<Addon> returnedAddons = mockService.filter("One","Names");

        //Assert
        Assert.assertEquals(3, returnedAddons.size());
    }

    @Test
    public void filterByUser_should_filterAddons_when_addonsContainCreator(){
        //Arrange
        Addon expectedAddon = createAddon();
        expectedAddon.setCreator("A");
        Addon unexpectedAddon = createAddon();
        unexpectedAddon.setCreator("B");
        List<Addon> expectedAddons = new ArrayList<>();
        expectedAddons.add(expectedAddon);
        expectedAddons.add(unexpectedAddon);

        //Act
        List<Addon> returnedAddons = mockService.filterByUser(expectedAddons,"A");

        //Assert
        Assert.assertEquals(1, returnedAddons.size());
    }

    @Test
    public void filterByStatus_should_filterAddons_when_addonsContainStatus(){
        //Arrange
        Set<Status> expectedStatuses = new HashSet<>();
        Status expectedStatus = createStatus("A");
        expectedStatuses.add(expectedStatus);
        Addon expectedAddon = createAddon();
        expectedAddon.setStatuses(expectedStatuses);

        Set<Status> unexpectedStatuses = new HashSet<>();
        Status unexpectedStatus = createStatus("B");
        unexpectedStatuses.add(unexpectedStatus);
        Addon unexpectedAddon = createAddon();
        unexpectedAddon.setStatuses(unexpectedStatuses);

        List<Addon> expectedAddons = new ArrayList<>();
        expectedAddons.add(expectedAddon);
        expectedAddons.add(unexpectedAddon);
        Mockito.when(statusService.getByName(anyString())).thenReturn(expectedStatus);

        //Act
        List<Addon> returnedAddons = mockService.filterByStatus(expectedAddons,"A");

        //Assert
        Assert.assertEquals(1, returnedAddons.size());
    }

    @Test
    public void filterByCreationDate_should_filterAddons_when_addonsAreCreatedAfterDate(){
        //Arrange
        Date expectedDate = new Date(System.currentTimeMillis());
        Addon expectedAddon = createAddon();
        expectedAddon.setUploadDate(expectedDate);

        Date unexpectedDate = new Date(System.currentTimeMillis()-WEEK);
        Addon unexpectedAddon = createAddon();
        unexpectedAddon.setUploadDate(unexpectedDate);

        List<Addon> expectedAddons = new ArrayList<>();
        expectedAddons.add(expectedAddon);
        expectedAddons.add(unexpectedAddon);

        //Act
        List<Addon> returnedAddons = mockService.filterByCreationDate(expectedAddons,DAY);

        //Assert
        Assert.assertEquals(1, returnedAddons.size());
    }

    @Test
    public void filterByDownloadCount_should_sortAddons_when_addonsWereDownloaded(){
        //Arrange
        Addon smallAddon = createAddon();
        smallAddon.setNumberOfDownloads(10);

        Addon bigAddon = createAddon();
        bigAddon.setNumberOfDownloads(10);

        Addon smallestAddon = createAddon();
        smallestAddon.setNumberOfDownloads(1);

        List<Addon> expectedAddons = new ArrayList<>();
        expectedAddons.add(smallAddon);
        expectedAddons.add(bigAddon);
        expectedAddons.add(smallestAddon);

        Integer expectedBiggestNumberOfDownloads = 10;

        //Act
        List<Addon> returnedAddons = mockService.sortByDownloadCount(expectedAddons);

        //Assert
        Assert.assertEquals(expectedBiggestNumberOfDownloads, returnedAddons.get(0).getNumberOfDownloads());
    }


    @Test
    public void getStatusNames_should_returnNames_when_addonsHaveStatuses(){
        //Arrange
        Set<Status> expectedStatuses = new HashSet<>();
        Status statusOne = createStatus("A");
        Status statusTwo = createStatus("B");
        Status statusThree = createStatus("C");
        expectedStatuses.add(statusOne);
        expectedStatuses.add(statusTwo);
        expectedStatuses.add(statusThree);
        Addon expectedAddon = createAddon();
        expectedAddon.setStatuses(expectedStatuses);
        Mockito.when(addonRepository.existsByName(anyString())).thenReturn(true);
        Mockito.when(addonRepository.findByName(anyString())).thenReturn(expectedAddon);

        //Act
        String returnedStatuses = mockService.getStatusNames(anyString());

        //Assert
//        Assert.assertEquals("[A, B, C]", returnedStatuses);
        Assert.assertEquals("[C, B, A]", returnedStatuses);
    }

    @Test
    public void getTagNames_should_returnTagsString_when_addonsContainTags(){
        //Arrange
        Set<Tag> expectedTags = new HashSet<>();
        Tag tagOne = createTag();
        Tag tagTwo = createTag();
        Tag tagThree = createTag();
        expectedTags.add(tagOne);
        expectedTags.add(tagTwo);
        expectedTags.add(tagThree);
        Addon expectedAddon = createAddon();
        expectedAddon.setTags(expectedTags);
        Mockito.when(addonRepository.existsByName(anyString())).thenReturn(true);
        Mockito.when(addonRepository.findByName(anyString())).thenReturn(expectedAddon);

        //Act
        String returnedStatuses = mockService.getTagNames(anyString());

        //Assert
        Assert.assertEquals("#Test Tag,#Test Tag,#Test Tag", returnedStatuses);
    }

    @Test
    public void getByUserAndStatus_should_returnAddons_when_match(){
        //Arrange
        Set<Status> expectedStatuses = new HashSet<>();
        Status expectedStatus = createStatus("Status");
        expectedStatuses.add(expectedStatus);
        Addon expectedAddon = createAddon();
        expectedAddon.setStatuses(expectedStatuses);
        expectedAddon.setCreator("Creator");
        List<Addon> expectedAddons = new ArrayList<>();
        expectedAddons.add(expectedAddon);
        Mockito.when(statusService.getByName(anyString())).thenReturn(expectedStatus);
        Mockito.when(addonRepository.findAll()).thenReturn(expectedAddons);

        //Act
        List<Addon> returnedAddons = mockService.getByUserAndStatus("Status","Creator");

        //Assert
        Assert.assertEquals(expectedAddon,returnedAddons.get(0));
    }

    @Test
    public void removeTagFromAddons_should_removeTags_when_addonsExist(){
        //Arrange
        Set<Tag> expectedTags = new HashSet<>();
        Tag tagOne = createTag();
        expectedTags.add(tagOne);
        Addon expectedAddon = createAddon();
        expectedAddon.setTags(expectedTags);
        List<Addon> expectedAddons = new ArrayList<>();
        expectedAddons.add(expectedAddon);
        Mockito.when(addonRepository.existsByName(anyString())).thenReturn(true);
        Mockito.when(addonRepository.findAll()).thenReturn(expectedAddons);

        //Act
        mockService.removeTagFromAddons(tagOne);

        //Assert
        Assert.assertEquals(0,expectedAddon.getTags().size());
    }

    @Test
    public void paginationWithSortByStatus_should_return_when_addonsExist() {
        //Arrange
        Addon sampleAddon = createAddon();
        List<Addon> expectedAddons = new ArrayList<>();
        expectedAddons.add(sampleAddon);
        Page<Addon> expectedPage = new PageImpl<>(expectedAddons);
        Mockito.when(addonRepository.findAllByStatuses_NameOrderByNumberOfDownloadsDesc(anyString(),any())).thenReturn(expectedPage);

        //Act
        Page<Addon> returnedPage = mockService.paginationWithSortByStatus("Number Of Downloads",0,createPageable());

        //Assert
        Assert.assertNotNull(returnedPage);
    }
}
